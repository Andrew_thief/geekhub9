package org.geekhub.game.exception;

public class IllegalRightException extends RuntimeException {
    public IllegalRightException(String message){
        super(message);
    }
}
