package org.geekhub.game.gameaction;

import org.geekhub.game.dice.DiceShard;
import org.geekhub.game.personage.Personage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GameActionResolver {

    private DiceShard diceShard;

    public GameActionResolver(DiceShard diceShard) {
        this.diceShard = diceShard;
    }

    public GameAction getGameAction(List<GameAction> actions) {
        int actionCount = actions.size();

        return actions.get(diceShard.throwDice(actionCount));
    }

    public Personage changeCharacteristics(GameAction action, Personage personage) {
        int health = personage.getHealth();
        int hunger = personage.getHealth();
        int mentalHeath = personage.getHealth();
        int actionPoints = personage.getActionCount();

        personage.setActionCount(actionPoints - 1);
        personage.setMentalHealth(mentalHeath + action.getMentalHealthReward());
        personage.setHunger(hunger + action.getHungerReward());
        personage.setHealth(health + action.getHealthReward());
        return personage;
    }

    public boolean isAlive(Personage personage) {
        if (personage.getHealth() <= 0 ||
                personage.getMentalHealth() <= 0 ||
                personage.getHunger() <= 0) {
            return false;
        }
        return true;
    }

}
