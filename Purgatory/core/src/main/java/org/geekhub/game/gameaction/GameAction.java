package org.geekhub.game.gameaction;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("game_action")
public class GameAction {

    private long id;
    private String description;

    private int hungerReward;
    private int mentalHealthReward;
    private int healthReward;

    @Id
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }

    public int getHungerReward() {
        return hungerReward;
    }

    public int getMentalHealthReward() {
        return mentalHealthReward;
    }

    public int getHealthReward() {
        return healthReward;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHungerReward(int hungerReward) {
        this.hungerReward = hungerReward;
    }

    public void setMentalHealthReward(int mentalHealthReward) {
        this.mentalHealthReward = mentalHealthReward;
    }

    public void setHealthReward(int healthReward) {
        this.healthReward = healthReward;
    }
}
