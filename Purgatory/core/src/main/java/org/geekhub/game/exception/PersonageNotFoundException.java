package org.geekhub.game.exception;

public class PersonageNotFoundException extends RuntimeException {
    public PersonageNotFoundException(String message) {
        super(message);
    }
}
