package org.geekhub.game.exception;

public class PersonageIsDeadException extends RuntimeException {
    public PersonageIsDeadException(String message) {
        super(message);
    }
}
