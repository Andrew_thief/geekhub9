package org.geekhub.game.location;

import org.geekhub.game.exception.LocationNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LocationService {

    private LocationRepository locationRepository;


    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public Location getLocationByLevel(int level) {
        Location location = locationRepository.getLocationByLevel(level);
        if (location == null) {
            throw new LocationNotFoundException("Something went wrong! " +
                    "Cannot load location");
        }
        return location;
    }
}
