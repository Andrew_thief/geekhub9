package org.geekhub.game.user;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationUserRepository extends CrudRepository<ApplicationUser, Long> {

    @Query("SELECT users.id AS id," +
            " users.email AS email," +
            " users.is_active AS is_active," +
            " users.last_name AS last_name," +
            " users.password AS password, " +
            "users.first_name AS first_name, " +
            "users.authority AS authority," +
            "personage.id AS personage_id," +
            " personage.type AS personage_type, " +
            "personage.level AS personage_level, " +
            "personage.hunger AS personage_hunger, " +
            "personage.health AS personage_health, " +
            "personage.defence AS personage_defence," +
            " personage.strength AS personage_strength," +
            " personage.dexterity AS personage_dexterity," +
            " personage.is_fighting AS personage_is_fighting, " +
            "personage.action_count AS personage_action_count, " +
            "personage.description AS personage_description, " +
            "personage.mental_health AS personage_mental_health " +
            "FROM users " +
            "LEFT OUTER JOIN personage AS personage ON personage.id = users.id" +
            " WHERE users.email = :email")
    ApplicationUser findByEmail(@Param("email") String email);

    @Modifying
    @Query("insert into users(first_name, last_name, email, password, is_active, authority) " +
            "values (:firstName, :lastName, :email, :password, :isActive, :authority)")
    void saveNew(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("email") String email,
            @Param("password") String password,
            @Param("isActive") boolean isActive,
            @Param("authority") String auth);

    @Query("select * from users where is_active = :is_active")
    List<ApplicationUser> findAllByActive(@Param("is_active") boolean isActive);

    @Query("select exists(select * from users where email = :email)")
    Boolean isEmailInUse(@Param("email") String email);
}
