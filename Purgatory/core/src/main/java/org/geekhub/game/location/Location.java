package org.geekhub.game.location;


import org.geekhub.game.gameaction.GameAction;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@Table("locations")
public class Location {

    private long id;
    private String name;
    private String description;
    private int locationLevel;
    private List<GameAction> gameAction;

    @Id
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getLocationLevel() {
        return locationLevel;
    }

    public void setLocationLevel(int locationLevel) {
        this.locationLevel = locationLevel;
    }

    public List<GameAction> getGameAction() {
        return gameAction;
    }

    public void setGameAction(List<GameAction> gameAction) {
        this.gameAction = gameAction;
    }
}
