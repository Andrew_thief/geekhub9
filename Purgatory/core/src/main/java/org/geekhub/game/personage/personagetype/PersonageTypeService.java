package org.geekhub.game.personage.personagetype;

import org.springframework.stereotype.Service;

@Service
public class PersonageTypeService {

    private PersonageTypeRepository personageTypeRepository;

    public PersonageTypeService(PersonageTypeRepository personageTypeRepository) {
        this.personageTypeRepository = personageTypeRepository;
    }

    public PersonageTypeInfo getPersonageInfoByType(PersonageType type) {
        return personageTypeRepository.findByPersonageType(type.name());

    }
}
