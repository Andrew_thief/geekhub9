package org.geekhub.game.user;

import org.geekhub.game.exception.EmailInUseException;
import org.geekhub.game.exception.IllegalOperationException;
import org.geekhub.game.exception.IllegalRightException;
import org.geekhub.game.exception.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.geekhub.game.user.Authority.*;

@Service
public class ApplicationUsersService {

    private final ApplicationUserRepository applicationUserRepo;

    public ApplicationUsersService(ApplicationUserRepository applicationUserRepo) {
        this.applicationUserRepo = applicationUserRepo;
    }

    public ApplicationUser saveUsersInfo(ApplicationUser user) {
        return applicationUserRepo.save(user);
    }

    public void saveNewUser(ApplicationUser user) {
        applicationUserRepo.saveNew(user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                user.isActive(),
                user.getAuthority().name());
    }

    public List<ApplicationUser> getByActive(boolean isActive) {
        List<ApplicationUser> users = applicationUserRepo.findAllByActive(isActive);
        if (users.isEmpty()) {
            throw new UserNotFoundException("Users with active [" + isActive + "]");
        } else {
            return users;
        }
    }


    public ApplicationUser getById(long id) {
        Optional<ApplicationUser> user = applicationUserRepo.findById(id);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new UserNotFoundException("User with id [" + id + "] not found");
        }
    }

    public boolean checkEmailUsage(String email) {
        Boolean isUsed = applicationUserRepo.isEmailInUse(email);
        if (isUsed) {
            throw new EmailInUseException("User with this email already defined in system. Please enter another email!");
        }
        return false;
    }


    public ApplicationUser getByEmail(String email) {
        ApplicationUser user = applicationUserRepo.findByEmail(email);
        if (user == null) {
            throw new UserNotFoundException("User with email [" + email + "] not found!");
        } else {
            return user;
        }
    }

    public void setAdminAuthority(ApplicationUser applicationUser) {
        Authority usersAuthority = applicationUser.getAuthority();
        if (usersAuthority.equals(ROLE_SUPER_ADMIN)) {
            throw new IllegalOperationException("Super admin authority cannot be modified");
        }

        applicationUser.setAuthority(ROLE_ADMIN);
        applicationUserRepo.save(applicationUser);
    }

    public void setUserAuthority(ApplicationUser applicationUser) {
        Authority usersAuthority = applicationUser.getAuthority();

        if (usersAuthority.equals(ROLE_SUPER_ADMIN)) {
            throw new IllegalOperationException("Super admin authority cannot be modified");
        }
        applicationUser.setAuthority(ROLE_USER);
        applicationUserRepo.save(applicationUser);
    }

    public void deleteUser(ApplicationUser user, Authority currentUserAuthority) {
        Authority authority = user.getAuthority();
        if (authority.equals(currentUserAuthority) || authority.equals(ROLE_SUPER_ADMIN)) {
            throw new IllegalRightException("You don`t have permission to delete this user!");
        }
        applicationUserRepo.delete(user);
    }

    public void setUsersActivity(ApplicationUser user, boolean activity, Authority currentUserAuthority) {
        Authority authority = user.getAuthority();

        if (authority.equals(currentUserAuthority) || authority.equals(ROLE_SUPER_ADMIN)) {
            throw new IllegalRightException("You don`t have permission to deactivate this user!");
        }

        user.setActive(activity);
        applicationUserRepo.save(user);

    }

}


