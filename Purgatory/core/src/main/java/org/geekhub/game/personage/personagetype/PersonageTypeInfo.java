package org.geekhub.game.personage.personagetype;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("character_type")
public class PersonageTypeInfo {

    private long id;
    private PersonageType type;
    private String description;

    private int hunger;
    private int mentalHealth;
    private int health;
    private int strength;
    private int dexterity;
    private int defence;

    private int actionCount;

    @Id
    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getHunger() {
        return hunger;
    }

    public int getHealth() {
        return health;
    }

    public int getActionCount() {
        return actionCount;
    }

    public PersonageType getType() {
        return type;
    }

    public int getMentalHealth() {
        return mentalHealth;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getDefence() {
        return defence;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setType(PersonageType type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setMentalHealth(int mentalHealth) {
        this.mentalHealth = mentalHealth;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public void setActionCount(int actionCount) {
        this.actionCount = actionCount;
    }
}
