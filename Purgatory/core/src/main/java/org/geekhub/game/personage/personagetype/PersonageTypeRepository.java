package org.geekhub.game.personage.personagetype;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonageTypeRepository extends CrudRepository<PersonageTypeInfo, Long> {

    @Query("select * from personage_type_info")
    List<PersonageTypeInfo> findAll();

    @Query("select * from personage_type_info where type = :personage_type")
    PersonageTypeInfo findByPersonageType(@Param("personage_type") String personageType);

}
