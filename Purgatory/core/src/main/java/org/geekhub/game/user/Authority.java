package org.geekhub.game.user;

public enum Authority {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}
