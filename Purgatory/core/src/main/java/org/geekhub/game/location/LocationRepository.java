package org.geekhub.game.location;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {

    @Query("select * from locations where location_level = :level")
    Location getLocationByLevel(@Param("level") int level);
}
