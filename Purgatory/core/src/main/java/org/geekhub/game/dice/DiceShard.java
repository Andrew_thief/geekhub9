package org.geekhub.game.dice;

import org.geekhub.game.exception.IllegalOperationException;
import org.springframework.stereotype.Component;

import java.util.Random;


@Component
public class DiceShard {

    private Random dice = new Random();

    public int throwDice(int max) {
        return dice.nextInt(max);
    }

    public int throwDice(int min, int max) {
        if (min > max) {
            throw new IllegalOperationException("Minimum value cannot be bigger than max");
        }

        int difference = max - min;

        int result = dice.nextInt(difference + 1);

        return result + min;
    }

}
