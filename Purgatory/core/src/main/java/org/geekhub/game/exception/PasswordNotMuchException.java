package org.geekhub.game.exception;

public class PasswordNotMuchException extends RuntimeException {
    public PasswordNotMuchException(String message){
        super(message);
    }
}
