CREATE TABLE mob
(
    id            SERIAL PRIMARY KEY,
    location      INTEGER REFERENCES locations (id),
    locations_key INTEGER,
    health        SMALLINT,
    damage        SMALLINT,
    defence       SMALLINT,

    item_id       SMALLINT
);