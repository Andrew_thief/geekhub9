--
-- Insertion of personage types for game
--


--Hollow Knight
INSERT INTO personage_type_info(type,
                                description,
                                hunger,
                                mental_health,
                                health,
                                strength,
                                dexterity,
                                defence,
                                action_count)
VALUES ('SAINT_KNIGHT',
        'description',
        10,
        100,
        120,
        7,
        3,
        7,
        8);


--Rogue
INSERT INTO personage_type_info(type,
                                description,
                                hunger,
                                mental_health,
                                health,
                                strength,
                                dexterity,
                                defence,
                                action_count)
VALUES ('ROGUE',
        'description',
        10,
        100,
        100,
        5,
        6,
        5,
        9);
