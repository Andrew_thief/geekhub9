--
-- Name: personage_type; Type: TABLE;
--

CREATE TABLE personage_type_info
(
    id            SERIAL PRIMARY KEY,
    type          VARCHAR(20) UNIQUE,
    description   VARCHAR(255),
    hunger        SMALLINT,
    mental_health SMALLINT,
    health        SMALLINT,
    strength      SMALLINT,
    dexterity     SMALLINT,
    defence       SMALLINT,
    action_count  SMALLINT
);

