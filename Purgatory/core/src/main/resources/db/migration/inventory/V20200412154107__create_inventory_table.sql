CREATE TABLE inventory
(
    id          SERIAL PRIMARY KEY,
    user_id     INTEGER,
    item_id     INTEGER,
    is_equipped boolean

);

ALTER TABLE inventory
    ADD FOREIGN KEY (user_id) references users;
ALTER TABLE inventory
    ADD FOREIGN KEY (item_id) references item