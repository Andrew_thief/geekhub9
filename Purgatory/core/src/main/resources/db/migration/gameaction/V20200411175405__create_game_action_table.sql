CREATE TABLE game_action
(
    id                   SERIAL PRIMARY KEY,
    location             INTEGER REFERENCES locations (id),
    locations_key        INTEGER,
    description          VARCHAR(255),
    hunger_reward        SMALLINT,
    mental_health_reward SMALLINT,
    health_reward        SMALLINT,
    item_id              INTEGER
);