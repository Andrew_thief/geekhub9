INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 0, 'You look at the water barrel both you. In this barrel you find only water. Nothing was changed )',
        0, 0, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 1, 'You find a Iron sword - standard weapon in this game. Congratulations',
        0, 10, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 2, 'You see a ghost in a door. You really scared... Mental health descries',
        0, -20, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 3, 'Angry dog bite your bag with items. You lost some food.',
        -25, -10, 0);
INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 4, 'In the dark room you dont see anything. You fall apart because you dont have any light',
        0, -10, 0);
INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 6, 'In the end of room you see something shiny. Its a lucky coin!',
        0, 15, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 7, 'Stranger gives you a health potion...',
        0, 0, 25);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 8, 'Mouses eat the part of your food...',
        -10, 0, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 9, 'You find some good pork meat',
        25, 0, 10);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 10, 'You see in the dark cats eyes...',
        0, 0, 0);

INSERT INTO game_action(location, locations_key, description, hunger_reward, mental_health_reward, health_reward)
VALUES (1, 11, 'Some scary voices you hear from the over side of walls, but you dont even care about. Mental health++',
        0, 15, 0);


