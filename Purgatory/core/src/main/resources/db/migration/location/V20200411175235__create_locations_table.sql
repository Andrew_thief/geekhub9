CREATE TABLE locations
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(50) UNIQUE NOT NULL,
    description VARCHAR(1023)      NOT NULL,
    location_level       INTEGER            NOT NULL
);
