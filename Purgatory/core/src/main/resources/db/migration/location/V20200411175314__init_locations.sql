-- insertion of  locations for game application

-- location: Dark room
INSERT INTO locations(name, description, location_level)
VALUES ('Dark room', 'In the dark room, even the ticking had a relaxed feeling, ' ||
                     'as if it was a heart-beat at rest. ' ||
                     'but this atmosphere was scattered by some scary voices and characters heart-beat was increased.' ||
                     'You fill really scared because in this dark you cant see anything.', 1);

-- location: Basement
INSERT INTO locations(name, description, location_level)
VALUES ('Basement', 'The basement is the only salvageable thing about the house.' ||
                    ' Constructed from quarry rock, its walls are thicker than a medieval castle,' ||
                    ' but the house on top was just wood. It is a warren of small rooms with only one way in or out, ' ||
                    'somehow another exit will have to be made. ' ||
                    'Close to the low ceiling, just under the rotting beams that suspend the floor above are windows,' ||
                    ' long and skinny, mostly covered in soil that lightened the darkness.', 2);
