--weapons
INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Iron Sword', 'Standard melee weapon', 'WEAPON', 0, 2, 1, 0, 0, 0);

INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Ricardo Rapier', 'Weapon of real gentleman. Perfect weapon for dogging', 'WEAPON', 0, 4, 5, 0, 0, 0);

INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Excalibur', 'Saint sword of King Artur. Deadly damage', 'WEAPON', 1, 7, 3, 0, 0, 0);


--armor
INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Skin Armor', 'Standard armor', 'ARMOR', 2, 0, 1, 0, 0, 0);

INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('The bag', 'Cover your dirty soul with dirty bag...', 'ARMOR', 1, 0, 2, 0, 0, 0);

INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('King Armor', 'Armor for the real king. Not for you... But you can wear it!', 'ARMOR', 5, 0, 3, 0, 0, 0);


--useful items
INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Health potion', 'Healing potion', 'USEFUL', 0, 0, 0, 20, 0, 0);



INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('A Story book', 'In this book described stories of first souls... Restore your mental health', 'USEFUL', 0, 0,
        0, 0, 0, 20);



INSERT INTO item(name, description, type, defence, strength, dexterity, health, hunger, mental_health)
VALUES ('Apple', 'Big red apple', 'USEFUL', 0, 0, 0, 0, 20, 0);



