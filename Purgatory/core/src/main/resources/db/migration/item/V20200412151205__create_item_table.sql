CREATE TABLE item
(
    id            serial primary key,
    name          VARCHAR(20),
    description   VARCHAR(255),
    type          varchar(20),

    defence       SMALLINT,
    strength      SMALLINT,
    dexterity     SMALLINT,

    health        SMALLINT,
    hunger        SMALLINT,
    mental_health SMALLINT
);