--
-- Insert to table application_user standard three users with different roles
--

INSERT INTO users(first_name, last_name, email, is_active, password, authority) VALUES ('Andrew', 'Maslyuk', 'super_admin@gmail.com', true, '$2a$10$aAKd9caed6xzg5VtkuoATuGW1MFBQR1Lb3cK6xboYsjRn1.0AfoIa', 'ROLE_SUPER_ADMIN');
INSERT INTO users(first_name, last_name, email, is_active, password, authority) VALUES ('Admin', 'Admin', 'admin@gmail.com', true, '$2a$10$mW5rJa6rSlLSzaTXjClN0OEczJEN0XpbamrmyMEoErsaSINRJG2Be', 'ROLE_ADMIN');
INSERT INTO users(first_name, last_name, email, is_active, password, authority) VALUES ('Andrew', 'Maslyuk', 'user@gmail.com', true, '$2a$10$Jh7Ymjt16ps0m6Nb0unzuuF1rCjhS/9QPaNigONFtqIoblSEq.u/W', 'ROLE_USER');


