--
-- Name: application_user; Type: TABLE;
--

CREATE TABLE users
(
    id           SERIAL PRIMARY KEY,
    email        VARCHAR(50) UNIQUE NOT NULL,
    first_name   VARCHAR(255)       NOT NULL,
    last_name    VARCHAR(255)       NOT NULL,
    is_active    BOOLEAN            NOT NULL DEFAULT TRUE,
    password     VARCHAR(255)       NOT NULL,
    personage_id INTEGER                     DEFAULT NULL,
    authority    VARCHAR(50)                 DEFAULT 'ROLE_USER',
    FOREIGN KEY (personage_id) REFERENCES personage (id)
);
