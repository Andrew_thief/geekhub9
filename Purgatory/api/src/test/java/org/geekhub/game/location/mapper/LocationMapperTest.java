package org.geekhub.game.location.mapper;

import org.geekhub.game.location.Location;
import org.geekhub.game.location.dto.LocationInfoDto;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LocationMapperTest {

    private LocationMapper locationMapper = new LocationMapper();


    @Test
    public void Should_Map_LocationInfoDto_When_To_location_Info() {
        Location location = new Location();

        location.setName("naem");
        location.setDescription("descs");
        location.setLocationLevel(3);

        LocationInfoDto object = locationMapper.toLocationInfo(location);

        assertEquals(object.getName(), location.getName());
        assertEquals(object.getDescription(), location.getDescription());


    }


}
