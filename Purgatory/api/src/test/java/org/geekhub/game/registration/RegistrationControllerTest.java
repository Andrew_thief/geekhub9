package org.geekhub.game.registration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.exception.EmailInUseException;
import org.geekhub.game.user.ApplicationUsersService;
import org.geekhub.game.user.RegistrationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(RegistrationController.class)
@AutoConfigureDataJdbc
public class RegistrationControllerTest extends AbstractTestNGSpringContextTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private DataSource dataSource;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @MockBean
    private ApplicationUsersService usersService;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @Autowired
    @MockBean
    private BCryptPasswordEncoder encoder;

    @BeforeMethod
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();
    }



    @Test
    public void Should_Expect_Status_BadRequest_When_Register_With_Not_Equals_Params() throws Exception {

        mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email", "email")
                .param("firstName", "fmnme")
                .param("lastName", "asfjasf")
                .param("pass1", "pass")
                .param("pass2", "pasas"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void Should_Expect_Status_BadRequest_When_Register_With_Email_In_Use() throws Exception {
        when(usersService.checkEmailUsage(any())).thenThrow(EmailInUseException.class);

        mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .param("email", "email")
                .param("firstName", "fmnme")
                .param("lastName", "asfjasf")
                .param("pass1", "pass")
                .param("pass2", "pasas"))
                .andExpect(status().isBadRequest());
    }
}
