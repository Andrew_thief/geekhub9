package org.geekhub.game.location;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@DataJdbcTest
public class LocationDataJdbcTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private LocationRepository locationRepository;

    private Location location;
    private static final String DESCRIPTION = "description";
    private static final String NAME = "location";
    private static final int LEVEL = 2;

    @BeforeMethod
    public void setUp() {
        locationRepository.deleteAll();
        location = new Location();
        location.setDescription(DESCRIPTION);
        location.setLocationLevel(LEVEL);
        location.setName(NAME);
    }

    @Test
    public void Should_Load_Location_When_Get_Location() {
        Location saved = locationRepository.save(location);
        assertThat(saved.getId()).isNotNull();

        Optional<Location> loaded = locationRepository.findById(location.getId());
        assertThat(loaded).isNotEmpty();

        Location loadedLocation = loaded.get();
        assertThat(loadedLocation.getDescription()).isEqualTo(DESCRIPTION);
        assertThat(loadedLocation.getLocationLevel()).isEqualTo(LEVEL);
        assertThat(loadedLocation.getName()).isEqualTo(NAME);
    }

    @Test
    public void Should_Load_Location_When_getByLevel() {
        Location saved = locationRepository.save(location);
        assertThat(saved.getId()).isNotNull();

        Location loaded = locationRepository.getLocationByLevel(saved.getLocationLevel());
        assertThat(loaded).isNotNull();

        assertThat(loaded.getName()).isEqualTo(saved.getName());
        assertThat(loaded.getDescription()).isEqualTo(saved.getDescription());
        assertThat(loaded.getLocationLevel()).isEqualTo(saved.getLocationLevel());
    }
}
