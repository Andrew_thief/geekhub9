package org.geekhub.game.location;

import org.geekhub.game.exception.LocationNotFoundException;
import org.geekhub.game.gameaction.GameAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureWebMvc
public class LocationServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private LocationService locationService;


    @Test
    public void Should_Found_Location_By_Level_When_Find_By_Level() {
        Location location = locationService.getLocationByLevel(1);

        assertThat(location).isNotNull();
    }

    @Test(expectedExceptions = LocationNotFoundException.class)
    public void Should_Throw_LocationNotFoundException_When_Level_Is_Not_Correct() {
        locationService.getLocationByLevel(0);
    }

    @Test
    public void Should_Load_Game_Actions_When_Find_By_Level() {
        Location location = locationService.getLocationByLevel(1);
        List<GameAction> actionList = location.getGameAction();

        assertThat(actionList).isNotEmpty();
    }


}
