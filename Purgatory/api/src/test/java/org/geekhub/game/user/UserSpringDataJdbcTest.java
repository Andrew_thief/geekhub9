package org.geekhub.game.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@DataJdbcTest
public class UserSpringDataJdbcTest extends AbstractTransactionalTestNGSpringContextTests {

    private static final String EMAIl = "email";
    private static final String PASS = "pass";
    private static final String FIRST = "first";
    private static final String LAST = "last";
    private static final boolean IS_ACTIVE = true;
    private static final Authority AUTHORITY = Authority.ROLE_USER;

    private ApplicationUser applicationUser;

    @Autowired
    private ApplicationUserRepository userRepository;


    @BeforeMethod
    public void setUp() {
        userRepository.deleteAll();

        applicationUser = new ApplicationUser();
        applicationUser.setEmail(EMAIl);
        applicationUser.setPassword(PASS);
        applicationUser.setLastName(FIRST);
        applicationUser.setFirstName(LAST);
        applicationUser.setActive(IS_ACTIVE);
        applicationUser.setAuthority(AUTHORITY);


    }

    @Test
    public void Should_Create_New_User_When_Save_User() {
        ApplicationUser saved = userRepository.save(applicationUser);
        assertThat(saved.getId()).isNotNull();

        Optional<ApplicationUser> loaded = userRepository.findById(saved.getId());

        assertThat(loaded).isNotEmpty();
        assertThat(loaded.get().getEmail()).isEqualTo(EMAIl);
    }

    @Test
    public void Should_Load_Application_User_When_Get_By_Email() {
        ApplicationUser saved = userRepository.save(applicationUser);
        assertThat(saved.getId()).isNotNull();

        ApplicationUser loaded = userRepository.findByEmail(saved.getEmail());
        assertThat(loaded).isNotNull();
    }

    @Test
    public void Should_Load_All_Active_Users_When_Find_All_By_Active() {
        ApplicationUser notActive = new ApplicationUser();

        notActive.setEmail("second");
        notActive.setPassword(PASS);
        notActive.setLastName(FIRST);
        notActive.setFirstName(LAST);
        notActive.setActive(false);

        ApplicationUser saved1 = userRepository.save(applicationUser);
        ApplicationUser saved2 = userRepository.save(notActive);

        List<ApplicationUser> loaded = userRepository.findAllByActive(true);
        assertThat(loaded).isNotEmpty();
        assertThat(loaded.size()).isEqualTo(1);

        assertThat(loaded.get(0).isActive()).isTrue();
        assertThat(loaded.get(0).getEmail()).isEqualTo(saved1.getEmail());
    }

    @Test
    public void Should_Return_True_When_Email_In_Use() {
        ApplicationUser saved = userRepository.save(applicationUser);

        assertThat(userRepository.isEmailInUse(saved.getEmail())).isTrue();
    }

    @Test
    public void Should_Return_False_When_Email_Not_In_Use() {
        ApplicationUser saved = userRepository.save(applicationUser);
        String newEmail = "newMail";

        assertThat(userRepository.isEmailInUse(newEmail)).isFalse();
    }
}
