package org.geekhub.game.user;

import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class AuthorityTest extends AbstractTestNGSpringContextTests {

    private MockMvc mockMvc;

    @Autowired
    @MockBean
    private ApplicationUsersService service;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @Autowired
    @MockBean
    private UserMapper userMapper;

    @BeforeMethod
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();
    }

    @Test
    public void Should_Send_Redirect_To_Login_Page_When_User_Unauthorized() throws Exception {
        mockMvc.perform(get("/game"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    public void Should_Give_Access_To_Site_When_User_is_Authorized() throws Exception {
        when(authProvider.authenticate(any())).thenReturn(testUser(Authority.ROLE_USER));

        mockMvc.perform(get("/users/me")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("USER")))
                .andExpect(status().isOk());

    }

    @Test
    public void Should_Not_Allow_User_To_Admin_Panel_When_User_Has_Role_User() throws Exception {
        mockMvc.perform(get("/admin/users")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("USER")))
                .andExpect(status().isForbidden());
    }

    @Test
    public void Should_Allow_User_To_Admin_Panel_When_User_Has_Role_Admin() throws Exception {
        when(authProvider.authenticate(any())).thenReturn(testUser(Authority.ROLE_ADMIN));

        mockMvc.perform(get("/admin/users")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Not_Allow_User_To_User_Access_Setters_When_User_Has_Role_Admin() throws Exception {
        mockMvc.perform(put("/admin/users/1/access/set-user")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("ADMIN")))
                .andExpect(status().isForbidden());

        mockMvc.perform(put("/admin/users/1/access/set-admin")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("ADMIN")))
                .andExpect(status().isForbidden());
    }

    @Test
    public void Should_Allow_User_To_User_Access_Setters_When_User_Has_Role_Super_Admin() throws Exception {
        when(authProvider.authenticate(any())).thenReturn(testUser(Authority.ROLE_SUPER_ADMIN));


        mockMvc.perform(put("/admin/users/1/access/set-user")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("SUPER_ADMIN")))
                .andExpect(status().isOk());

        mockMvc.perform(put("/admin/users/1/access/set-admin")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user("test").password("test").roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    private ApplicationUser testUser(Authority auth) {
        ApplicationUser user = new ApplicationUser();
        user.setEmail("test");
        user.setPassword("test");
        user.setLastName("lastname");
        user.setFirstName("firstname");
        user.setActive(true);
        user.setAuthority(auth);
        return user;
    }

}
