package org.geekhub.game.user.mapper;


import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.Authority;
import org.geekhub.game.user.dto.CreatigUserDto;
import org.geekhub.game.user.dto.ProfileDto;
import org.geekhub.game.user.dto.WidenedProfileDto;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class UserMapperTest {


    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String PASS = "pass";
    private static final String EMAIL = "testMail";

    private UserMapper userMapper = new UserMapper();

    @Test
    public void Should_Map_From_Creating_User_Dto_To_Application_User() {
        CreatigUserDto object = new CreatigUserDto();
        object.setFirstName(FIRST_NAME);
        object.setLastName(LAST_NAME);
        object.setEmail(EMAIL);
        object.setPass1(PASS);
        object.setPass2(PASS);

        ApplicationUser newUser = userMapper.toEntity(object);

        assertEquals(newUser.getEmail(), EMAIL);
        assertEquals(newUser.getFirstName(), FIRST_NAME);
        assertEquals(newUser.getLastName(), LAST_NAME);
        assertEquals(newUser.getPassword(), PASS);

    }

    @Test
    public void Should_Map_From_Application_User_To_ProfileDto() {
        ApplicationUser entity = new ApplicationUser();
        entity.setFirstName(FIRST_NAME);
        entity.setLastName(LAST_NAME);
        entity.setEmail(EMAIL);
        entity.setPassword(PASS);

        ProfileDto object = userMapper.toProfile(entity);

        assertEquals(object.getEmail(), EMAIL);
        assertEquals(object.getFirstName(), FIRST_NAME);
        assertEquals(object.getLastName(), LAST_NAME);
    }

    @Test
    public void Should_Map_From_Entity_To_Widene_Profile() {
        ApplicationUser entity = new ApplicationUser();
        entity.setId(1);
        entity.setFirstName(FIRST_NAME);
        entity.setLastName(LAST_NAME);
        entity.setEmail(EMAIL);
        entity.setActive(true);
        entity.setAuthority(Authority.ROLE_USER);

        WidenedProfileDto object = userMapper.toWidenedProfile(entity);

        assertEquals(object.getId(), entity.getId());
        assertEquals(object.getEmail(), entity.getEmail());
        assertEquals(object.getFirstName(), entity.getFirstName());
        assertEquals(object.getLastName(), entity.getLastName());
        assertEquals(object.isActive(), entity.isActive());
        assertEquals(object.getAuthority(), entity.getAuthority());
    }
}
