package org.geekhub.game.user;

import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.exception.EmailInUseException;
import org.geekhub.game.user.dto.ProfileDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(UsersController.class)
@AutoConfigureDataJdbc
public class UsersControllerTest extends AbstractTestNGSpringContextTests {

    private final String TEST_EMAIL = "test-email";
    private final String TEST_FIRST_NAME = "test-first-name";
    private final String TEST_LAST_NAME = "test-last-name";
    private final String TEST_PASSWORD = "test-password";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private DataSource dataSource;

    @Autowired
    @MockBean
    private ApplicationUsersService usersService;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @Autowired
    @MockBean
    private BCryptPasswordEncoder encoder;

    @BeforeMethod
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();

        when(authProvider.authenticate(any())).thenReturn(testUser());
    }


    @Test
    public void Should_Expect_Status_Ok_When_Change_Email() throws Exception {
        mockMvc.perform(post("/users/me/email")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("USER"))
                .param("email", "newEmail"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Bad_Request_When_Change_Email_With_Used_email() throws Exception {
        when(usersService.checkEmailUsage("email")).thenThrow(EmailInUseException.class);

        mockMvc.perform(post("/users/me/email")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("email", "email"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Change_Password_With_Equals_Params() throws Exception {

        mockMvc.perform(put("/users/me/password")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("USER"))
                .param("pass1", "1")
                .param("pass2", "1"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_BAd_Request_When_Change_Password_With__Not_Equals_Params() throws Exception {

        mockMvc.perform(put("/users/me/password")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("pass1", "1")
                .param("pass2", "2"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void Should_Expect_Status_Ok_When_Change_First_Name() throws Exception {

        mockMvc.perform(put("/users/me/first-name")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("USER"))
                .param("firstName", "name"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Change_Last_Name() throws Exception {

        mockMvc.perform(put("/users/me/last-name")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("USER"))
                .param("lastName", "name"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Delete_User() throws Exception {

        mockMvc.perform(delete("/users/me")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("USER")))
                .andExpect(status().isOk());
    }


    private ApplicationUser testUser() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail(TEST_EMAIL);
        user.setPassword(TEST_PASSWORD);
        user.setLastName(TEST_FIRST_NAME);
        user.setFirstName(TEST_LAST_NAME);
        user.setActive(true);
        user.setAuthority(Authority.ROLE_USER);
        return user;
    }

    private ProfileDto getProfileDto() {
        ProfileDto profileDto = new ProfileDto();
        profileDto.setEmail(TEST_EMAIL);
        profileDto.setFirstName(TEST_FIRST_NAME);
        profileDto.setLastName(TEST_LAST_NAME);

        return profileDto;
    }


}
