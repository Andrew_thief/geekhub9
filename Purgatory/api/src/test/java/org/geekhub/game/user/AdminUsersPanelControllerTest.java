package org.geekhub.game.user;


import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.exception.EmailInUseException;
import org.geekhub.game.exception.UserNotFoundException;
import org.geekhub.game.user.dto.ProfileDto;
import org.geekhub.game.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(AdminUsersPanelController.class)
@AutoConfigureDataJdbc
public class AdminUsersPanelControllerTest extends AbstractTestNGSpringContextTests {

    private final String TEST_EMAIL = "test-email";
    private final String TEST_FIRST_NAME = "test-first-name";
    private final String TEST_LAST_NAME = "test-last-name";
    private final String TEST_PASSWORD = "test-password";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @MockBean
    private DataSource dataSource;

    @MockBean
    private UserMapper mapper;

    @Autowired
    @MockBean
    private ApplicationUsersService usersService;

    @Autowired
    @MockBean
    private BCryptPasswordEncoder encoder;

    @BeforeMethod
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();

        when(authProvider.authenticate(any())).thenReturn(testAdmin());
        when(usersService.getById(anyLong())).thenReturn(testUser());
    }


    @Test
    public void Should_Expect_Status_Ok_When_Change_Email() throws Exception {
        mockMvc.perform(post("/admin/users/3/email")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("email", "newEmail"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Change_Password_With_Equals_Params() throws Exception {

        mockMvc.perform(put("/admin/users/3/password")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("pass1", "1")
                .param("pass2", "1"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_BadRequest_When_Register_With_Not_Equals_Params() throws Exception {

        mockMvc.perform(post("/admin/users/add")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("email", "email")
                .param("firstName", "fmnme")
                .param("lastName", "asfjasf")
                .param("pass1", "pass")
                .param("pass2", "passs"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void Should_Expect_Status_BadRequest_When_Register_With_Email_In_Use() throws Exception {
        when(usersService.checkEmailUsage("email")).thenThrow(EmailInUseException.class);

        mockMvc.perform(post("/admin/users/add")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("email", "email")
                .param("firstName", "fmnme")
                .param("lastName", "asfjasf")
                .param("pass1", "pass")
                .param("pass2", "pass"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void Should_Expect_Status_BadRequest_When_Change_Password_With_Not_Equals_Params() throws Exception {

        mockMvc.perform(put("/admin/users/3/password")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("pass1", "1")
                .param("pass2", "2"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Change_First_Name() throws Exception {

        mockMvc.perform(put("/admin/users/3/first-name")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("firstName", "name"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Change_Last_Name() throws Exception {

        mockMvc.perform(put("/admin/users/3/last-name")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN"))
                .param("lastName", "name"))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Delete_User() throws Exception {

        mockMvc.perform(delete("/admin/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Set_Admin_Role() throws Exception {
        mockMvc.perform(put("/admin/users/3/access/set-admin")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Set_User_Role() throws Exception {
        mockMvc.perform(put("/admin/users/3/access/set-user")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Activate_User() throws Exception {
        mockMvc.perform(put("/admin/users/3/activate")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Disable_User() throws Exception {
        mockMvc.perform(put("/admin/users/3/disable")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Get_User_Info() throws Exception {
        mockMvc.perform(get("/admin/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Not_Found_When_Get_User_Info() throws Exception {
        when(usersService.getById(7)).thenThrow(UserNotFoundException.class);

        mockMvc.perform(get("/admin/users/7")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void Should_Expect_Status_Ok_When_Get_Users_Disabled() throws Exception {
        mockMvc.perform(get("/admin/users/disabled")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }


    private ApplicationUser testUser() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail("email");
        user.setPassword("password");
        user.setLastName("lastname");
        user.setFirstName("firstname");
        user.setActive(true);
        user.setAuthority(Authority.ROLE_USER);
        return user;
    }

    private ApplicationUser testAdmin() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail(TEST_EMAIL);
        user.setPassword(TEST_PASSWORD);
        user.setLastName(TEST_LAST_NAME);
        user.setFirstName(TEST_FIRST_NAME);
        user.setActive(true);
        user.setAuthority(Authority.ROLE_USER);
        return user;
    }


    private ProfileDto getProfileDto(ApplicationUser user) {
        ProfileDto profileDto = new ProfileDto();
        profileDto.setEmail(user.getEmail());
        profileDto.setFirstName(user.getFirstName());
        profileDto.setLastName(user.getLastName());

        return profileDto;
    }

}

