package org.geekhub.game.gameaction;

import org.geekhub.game.dice.DiceShard;
import org.geekhub.game.personage.Personage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class GameActionResolverTest {

    private GameActionResolver gameActionResolver;

    @BeforeMethod
    public void setUp() {
        gameActionResolver = new GameActionResolver(new DiceShard());
    }

    @Test
    public void Should_Return_True_Than_Personage_Is_Alive() {
        Personage personage = new Personage();
        personage.setHealth(1);
        personage.setMentalHealth(1);
        personage.setHunger(1);
        assertTrue(gameActionResolver.isAlive(personage));
    }

    @Test
    public void Should_Return_False_Than_Personage_Is_Alive() {
        Personage personage = new Personage();
        personage.setHealth(0);
        personage.setMentalHealth(1);
        personage.setHunger(1);
        assertFalse(gameActionResolver.isAlive(personage));
    }

    @Test
    public void Should_Return_One_Of_Game_Action_When_Get_Game_Action() {
        GameAction action = new GameAction();
        action.setId(1);
        List<GameAction> gameActions = List.of(action);

        GameAction newAction = gameActionResolver.getGameAction(gameActions);

        assertEquals(newAction.getId(), action.getId());
    }


    @Test
    public void Should_Change_Characteristics() {
        Personage personage = getPersonage();
        int health = personage.getHealth();
        int hunger = personage.getHealth();
        int mentalHeath = personage.getHealth();
        int actionPoints = personage.getActionCount();

        personage = gameActionResolver.changeCharacteristics(getAction(), personage);

        assertNotEquals(health, personage.getHealth());
        assertNotEquals(hunger, personage.getHealth());
        assertNotEquals(mentalHeath, personage.getHealth());
        assertNotEquals(actionPoints, personage.getActionCount());

    }

    private GameAction getAction() {
        GameAction gameAction = new GameAction();
        gameAction.setHealthReward(1);
        gameAction.setHungerReward(1);
        gameAction.setMentalHealthReward(1);
        gameAction.setDescription("dsae");

        return gameAction;
    }

    private Personage getPersonage() {
        Personage personage = new Personage();
        personage.setHunger(1);
        personage.setHealth(1);
        personage.setMentalHealth(1);
        personage.setStrength(1);
        personage.setDexterity(1);
        personage.setDefence(1);
        personage.setActionCount(1);
        personage.setLevel(1);

        return personage;
    }
}
