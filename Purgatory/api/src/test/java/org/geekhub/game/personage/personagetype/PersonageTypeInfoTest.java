package org.geekhub.game.personage.personagetype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureWebMvc
public class PersonageTypeInfoTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private PersonageTypeService personageTypeService;


    @Test
    public void Should_Load_Personage_Type_Info_When_Get_By_Type() {
        PersonageTypeInfo personageTypeInfo = personageTypeService.getPersonageInfoByType(PersonageType.ROGUE);

        assertThat(personageTypeInfo).isNotNull();
    }


}
