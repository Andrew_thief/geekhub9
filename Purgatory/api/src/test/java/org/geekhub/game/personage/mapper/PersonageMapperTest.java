package org.geekhub.game.personage.mapper;

import org.geekhub.game.personage.Personage;
import org.geekhub.game.personage.dto.PersonageDto;
import org.geekhub.game.personage.personagetype.PersonageType;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PersonageMapperTest {

    private PersonageMapper personageMapper = new PersonageMapper();

    @Test
    public void Should_Map_PersonageDto_When_toPersonageDto() {
        Personage personage = new Personage();

        personage.setType(PersonageType.ROGUE);
        personage.setDescription("descript");
        personage.setHunger(1);
        personage.setMentalHealth(1);
        personage.setHealth(2);
        personage.setStrength(4);
        personage.setDexterity(3);
        personage.setDefence(7);
        personage.setActionCount(3);

        PersonageDto object = personageMapper.toPersonageDto(personage);

        assertEquals(object.getHealth(), personage.getHealth());
        assertEquals(object.getDexterity(), personage.getDexterity());
        assertEquals(object.getDefence(), personage.getDefence());
        assertEquals(object.getStrength(), personage.getStrength());
        assertEquals(object.getMentalHealth(), personage.getMentalHealth());
        assertEquals(object.getHunger(), personage.getHunger());
        assertEquals(object.getLevel(), personage.getLevel());

    }

}
