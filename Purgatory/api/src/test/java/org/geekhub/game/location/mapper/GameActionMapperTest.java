package org.geekhub.game.location.mapper;

import org.geekhub.game.location.dto.GameActionInfoDto;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GameActionMapperTest {


    private GameActionMapper gameActionMapper = new GameActionMapper();


    @Test
    public void Should_Map_GameActionInfo_When_To_Game_Action_Info() {
        String description = " description";
        int health = 1;
        int mentalHealth = 2;
        int hunger = 3;
        boolean isAlive = true;

        GameActionInfoDto object = gameActionMapper.toGameActionInfo(description, health, mentalHealth, hunger, isAlive);

        assertEquals(object.getDescription(), description);
        assertEquals(object.getHealth(), health);
        assertEquals(object.getHunger(), hunger);
        assertEquals(object.getMentalHealth(), mentalHealth);
        assertEquals(object.isAlive(), isAlive);
    }


}
