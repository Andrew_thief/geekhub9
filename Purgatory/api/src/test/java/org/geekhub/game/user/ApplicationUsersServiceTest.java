package org.geekhub.game.user;

import org.geekhub.game.exception.EmailInUseException;
import org.geekhub.game.exception.IllegalOperationException;
import org.geekhub.game.exception.IllegalRightException;
import org.geekhub.game.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@SpringBootTest
@AutoConfigureWebMvc
public class ApplicationUsersServiceTest extends AbstractTestNGSpringContextTests {

    private static final String USED_EMAIL = "email";

    @Autowired
    private ApplicationUsersService usersService;

    @Autowired
    private ApplicationUserRepository repository;

    @BeforeMethod
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void Should_Return_User_By_Used_Id_When_Get_By_Id() {
        ApplicationUser user = usersService.saveUsersInfo(createUser(Authority.ROLE_USER));
        ApplicationUser loaded = usersService.getById(user.getId());
    }


    @Test(expectedExceptions = UserNotFoundException.class)
    public void Should_Throw_UserNotFoundException_When_Get_By_Not_Used_Id() {
        ApplicationUser user = usersService.saveUsersInfo(createUser(Authority.ROLE_USER));

        usersService.getById(12312);
    }

    @Test
    public void Should_Get_Active_Users_When_Get_By_Active_With_True() {
        usersService.saveUsersInfo(createUser(Authority.ROLE_USER));

        List<ApplicationUser> loaded = usersService.getByActive(true);
        assertThat(loaded.get(0).isActive()).isTrue();
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void Should_Throw_UserNotFoundException_When_Get_By_Active_Without_Active() {
        usersService.saveUsersInfo(createUser(Authority.ROLE_USER));

        List<ApplicationUser> loaded = usersService.getByActive(false);
        assertThat(loaded.get(0).isActive()).isTrue();
    }

    @Test(expectedExceptions = EmailInUseException.class)
    public void Should_Throw_EmailInUseException_When_Email_In_Use() {
        usersService.saveUsersInfo(createUser(Authority.ROLE_ADMIN));

        usersService.checkEmailUsage(USED_EMAIL);
    }

    @Test
    public void Should_Return_False_When_Email_Not_In_Use() {
        usersService.saveUsersInfo(createUser(Authority.ROLE_ADMIN));

        usersService.checkEmailUsage("anotherEmail");
    }

    @Test
    public void Should_Set_Admin_Role_When_Set_Admin_Authority() {
        ApplicationUser user = createUser(Authority.ROLE_USER);

        usersService.setAdminAuthority(user);
    }


    @Test
    public void Should_Set_User_Role_When_Set_User_Authority() {
        ApplicationUser user = createUser(Authority.ROLE_ADMIN);

        usersService.setAdminAuthority(user);
    }

    @Test(expectedExceptions = IllegalOperationException.class)
    public void Should_Throw_IllegalOperationException_When_Set_Admin_Authority_To_Super_Admin() {
        ApplicationUser user = createUser(Authority.ROLE_SUPER_ADMIN);

        usersService.setAdminAuthority(user);
    }


    @Test(expectedExceptions = IllegalOperationException.class)
    public void Should_Throw_IllegalOperationException_When_Set_User_Authority_To_Super_Admin() {
        ApplicationUser user = createUser(Authority.ROLE_SUPER_ADMIN);

        usersService.setAdminAuthority(user);
    }

    @Test
    public void Should_Delete_User_When_Delete_User() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_USER));

        usersService.deleteUser(applicationUser, Authority.ROLE_ADMIN);
    }


    @Test(expectedExceptions = IllegalRightException.class)
    public void Should_Throw_IllegalRightException_When_Delete_Users_With_Same_Authority() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_ADMIN));

        usersService.deleteUser(applicationUser, Authority.ROLE_ADMIN);
    }

    @Test(expectedExceptions = IllegalRightException.class)
    public void Should_Throw_IllegalRightException_When_Delete_User_With_Super_Admin_Authority() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_SUPER_ADMIN));

        usersService.deleteUser(applicationUser, Authority.ROLE_SUPER_ADMIN);
    }

    @Test
    public void Should_Set_Authority_When_User_Has_Permission() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_USER));

        usersService.setUsersActivity(applicationUser, false, Authority.ROLE_ADMIN);
    }


    @Test(expectedExceptions = IllegalRightException.class)
    public void Should_Throw_IllegalRightException_When_Set_Activity_To_Users_Has_Same_Authority() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_ADMIN));

        usersService.setUsersActivity(applicationUser, false, Authority.ROLE_ADMIN);
    }

    @Test(expectedExceptions = IllegalRightException.class)
    public void Should_Throw_IllegalRightExceptionWhen_Set_Activity_To_Super_Admin_Authority() {
        ApplicationUser applicationUser = usersService.saveUsersInfo(createUser(Authority.ROLE_SUPER_ADMIN));

        usersService.setUsersActivity(applicationUser, false, Authority.ROLE_SUPER_ADMIN);
    }


    private ApplicationUser createUser(Authority auth) {
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setFirstName("sadja");
        applicationUser.setLastName("asdasd");
        applicationUser.setPassword("sadasd");
        applicationUser.setActive(true);
        applicationUser.setEmail(USED_EMAIL);
        applicationUser.setAuthority(auth);

        return applicationUser;
    }

}
