package org.geekhub.game.personage;

import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUsersService;
import org.geekhub.game.user.Authority;
import org.geekhub.game.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ActiveProfiles("test")
@WebMvcTest(PersonageController.class)
@AutoConfigureDataJdbc

public class PersonageControllerTest extends AbstractTestNGSpringContextTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @MockBean
    private DataSource dataSource;

    @MockBean
    private UserMapper mapper;

    @Autowired
    @MockBean
    private ApplicationUsersService usersService;

    @Autowired
    @MockBean
    private BCryptPasswordEncoder encoder;

    @BeforeMethod
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();

        when(authProvider.authenticate(any())).thenReturn(testUser());
    }


    private ApplicationUser testUser() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail("email");
        user.setPassword("password");
        user.setLastName("lastname");
        user.setFirstName("firstname");
        user.setActive(true);
        user.setAuthority(Authority.ROLE_ADMIN);
        return user;
    }


}
