package org.geekhub.game.dice;

import org.geekhub.game.exception.IllegalOperationException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class DiceShardTest {

    private DiceShard diceShard;

    @BeforeMethod
    public void setUp() {
        diceShard = new DiceShard();
    }

    @Test
    public void Should_Return_Integer_Num_In_Diapason_From_Null_To_Max_When_Throw_Dice() {
        int max = 10;

        int result = diceShard.throwDice(max);
        assertTrue(result >= 0);
        assertTrue((result < 10));
    }


    @Test(expectedExceptions = IllegalOperationException.class)
    public void Should_Throw_IllegalOperationException_When_Min_Bigger_Than_Max() {
        int max = 10;
        int min = 12;
        assertTrue(min > max);

        diceShard.throwDice(min, max);
    }

    @Test
    public void Should_Return_Result_in_Diapason_When_Throw_Dice() {
        int max = 10;
        int min = 3;
        int result = diceShard.throwDice(min, max);

        assertTrue(result >= min);
        assertTrue(result <= max);
    }

}
