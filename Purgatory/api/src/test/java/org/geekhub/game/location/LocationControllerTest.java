package org.geekhub.game.location;

import org.geekhub.game.config.security.AuthenticateFilter;
import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.geekhub.game.gameaction.GameAction;
import org.geekhub.game.gameaction.GameActionResolver;
import org.geekhub.game.location.dto.LocationInfoDto;
import org.geekhub.game.location.mapper.LocationMapper;
import org.geekhub.game.personage.Personage;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUsersService;
import org.geekhub.game.user.Authority;
import org.geekhub.game.user.UsersController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.AutoConfigureDataJdbc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(UsersController.class)
@AutoConfigureDataJdbc
public class LocationControllerTest extends AbstractTestNGSpringContextTests {

    private final String TEST_EMAIL = "test-email";
    private final String TEST_FIRST_NAME = "test-first-name";
    private final String TEST_LAST_NAME = "test-last-name";
    private final String TEST_PASSWORD = "test-password";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private DataSource dataSource;

    @Autowired
    @MockBean
    private ApplicationUsersService usersService;

    @Autowired
    @MockBean
    private LocationService locationService;

    @Autowired
    @MockBean
    private AuthenticateUserProvider authProvider;

    @Autowired
    @MockBean
    private LocationMapper locationMapper;

    @Autowired
    @MockBean
    private GameActionResolver gameActionResolver;

    @Autowired
    @MockBean
    private BCryptPasswordEncoder encoder;

    @BeforeMethod
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .addFilter(new AuthenticateFilter(authProvider))
                .build();

        when(authProvider.authenticate(any())).thenReturn(testUserWithPersonage());
    }

    @Test
    public void Should_Expect_Status_Not_Found__Get_Location_By_Level() throws Exception {
        when(authProvider.authenticate(any())).thenReturn(testUser());

        mockMvc.perform(get("/game/location/info")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isNotFound());
    }

    @Test
    public void Should_Expect_Status_Is_Ok_Get_Location_By_Level() throws Exception {
        when(locationMapper.toLocationInfo(any())).thenReturn(new LocationInfoDto());

        mockMvc.perform(get("/game/location/info")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Is_Ok_When_Do_Action_With_Not_Alive() throws Exception {
        when(gameActionResolver.isAlive(any())).thenReturn(false);

        mockMvc.perform(post("/game/location/action")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Expect_Status_Is_ok_When_Do_Action() throws Exception {
        ApplicationUser user = testUserWithPersonage();

        when(authProvider.authenticate(any())).thenReturn(user);
        when(gameActionResolver.isAlive(any())).thenReturn(true);
        GameAction gameAction = new GameAction();
        gameAction.setHealthReward(1);
        gameAction.setHungerReward(1);
        gameAction.setMentalHealthReward(1);
        gameAction.setDescription("dsae");
        Location location = new Location();
        location.setGameAction(List.of(gameAction));


        when(gameActionResolver.getGameAction(anyList())).thenReturn(gameAction);
        when(locationService.getLocationByLevel(user.getPersonage().getLevel())).thenReturn( location);

        mockMvc.perform(post("/game/location/action")
                .contentType(MediaType.APPLICATION_JSON)
                .with(user(TEST_EMAIL).password(TEST_PASSWORD).roles("SUPER_ADMIN")))
                .andExpect(status().isOk());
    }


    private ApplicationUser testUser() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail("email");
        user.setPassword("password");
        user.setLastName("lastname");
        user.setFirstName("firstname");
        user.setActive(true);
        user.setAuthority(Authority.ROLE_USER);
        return user;
    }


    private ApplicationUser testUserWithPersonage() {
        ApplicationUser user = new ApplicationUser();
        user.setEmail("email");
        user.setPassword("password");
        user.setLastName("lastname");
        user.setFirstName("firstname");
        user.setActive(true);
        user.setAuthority(Authority.ROLE_USER);

        Personage personage = new Personage();
        personage.setHunger(1);
        personage.setHealth(1);
        personage.setMentalHealth(1);
        personage.setStrength(1);
        personage.setDexterity(1);
        personage.setDefence(1);
        personage.setActionCount(1);
        personage.setLevel(1);
        user.setPersonage(personage);
        return user;
    }

}


