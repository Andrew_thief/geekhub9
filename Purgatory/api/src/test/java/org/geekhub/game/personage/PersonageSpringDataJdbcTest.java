package org.geekhub.game.personage;


import org.geekhub.game.personage.personagetype.PersonageType;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUserRepository;
import org.geekhub.game.user.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("dev")
@DataJdbcTest
public class PersonageSpringDataJdbcTest extends AbstractTransactionalTestNGSpringContextTests {


    private static final PersonageType PERSONAGE_TYPE = PersonageType.ROGUE;
    private static final String PERSONAGE_DESCRIPTION = "test";
    private static final int ACTION_COUNT = 1;
    private static final int DEFENCE = 2;
    private static final int DEXTERITY = 3;
    private static final int STRENGTH = 4;
    private static final int HEALTH = 5;
    private static final int MENTAL_HEALTH = 6;
    private static final boolean IS_FIGHTING = false;
    private static final int LEVEL = 7;
    private static final int HUNGER = 8;

    private ApplicationUser applicationUser;
    private Personage personage;

    @Autowired
    private ApplicationUserRepository userRepository;

    @BeforeMethod
    public void setUp() {
        userRepository.deleteAll();

        applicationUser = new ApplicationUser();
        applicationUser.setEmail("email");
        applicationUser.setPassword("pass");
        applicationUser.setLastName("fName");
        applicationUser.setFirstName("lName");
        applicationUser.setActive(true);
        applicationUser.setAuthority(Authority.ROLE_USER);

        personage = new Personage();
        personage.setActionCount(ACTION_COUNT);
        personage.setType(PERSONAGE_TYPE);
        personage.setDescription(PERSONAGE_DESCRIPTION);
        personage.setDefence(DEFENCE);
        personage.setStrength(STRENGTH);
        personage.setDexterity(DEXTERITY);
        personage.setLevel(LEVEL);
        personage.setHealth(HEALTH);
        personage.setMentalHealth(MENTAL_HEALTH);
        personage.setFighting(IS_FIGHTING);
        personage.setHunger(HUNGER);

        ApplicationUser saved = userRepository.save(applicationUser);
        assertThat(saved.getId()).isNotNull();
    }


    @Test
    public void Should_Not_Throw_Any_Exception_CreateNewPersonageForUser() {
        applicationUser.setPersonage(personage);
        userRepository.save(applicationUser);

        Optional<ApplicationUser> loadedUser = userRepository.findById(applicationUser.getId());
        assertThat(loadedUser).isNotEmpty();

        Personage loaded = loadedUser.get().getPersonage();

        assertThat(loaded).isNotNull();
        assertThat(loaded.getActionCount()).isEqualTo(ACTION_COUNT);
        assertThat(loaded.getType()).isEqualTo(PERSONAGE_TYPE);
        assertThat(loaded.getDescription()).isEqualTo(PERSONAGE_DESCRIPTION);
        assertThat(loaded.getDefence()).isEqualTo(DEFENCE);
        assertThat(loaded.getStrength()).isEqualTo(STRENGTH);
        assertThat(loaded.getDexterity()).isEqualTo(DEXTERITY);
        assertThat(loaded.getLevel()).isEqualTo(LEVEL);
        assertThat(loaded.getHealth()).isEqualTo(HEALTH);
        assertThat(loaded.getMentalHealth()).isEqualTo(MENTAL_HEALTH);
        assertThat(loaded.isFighting()).isEqualTo(IS_FIGHTING);
        assertThat(loaded.getHunger()).isEqualTo(HUNGER);
    }

    @Test
    public void Should_Not_Throw_Any_Exception_When_DeletePersonageForUser() {
        applicationUser.setPersonage(personage);
        userRepository.save(applicationUser);

        Optional<ApplicationUser> loadedUser = userRepository.findById(applicationUser.getId());
        assertThat(loadedUser).isNotEmpty();

        applicationUser = loadedUser.get();
        applicationUser.setPersonage(null);
        userRepository.save(applicationUser);
    }


    @Test
    public void Should_Load_Application_User_With_Personage_When_Find_By_Email() {
        applicationUser.setPersonage(personage);
        ApplicationUser saved = userRepository.save(applicationUser);
        assertThat(saved.getId()).isNotNull();

        ApplicationUser loaded = userRepository.findByEmail(saved.getEmail());
        assertThat(loaded).isNotNull();

        assertThat(loaded.getPersonage()).isNotNull();
        assertThat(loaded.getPersonage().getHealth()).isEqualTo(personage.getHealth());
    }

}
