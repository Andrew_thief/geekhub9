package org.geekhub.game.personage.mapper;

import org.geekhub.game.personage.Personage;
import org.geekhub.game.personage.dto.PersonageTypeInfoDto;
import org.geekhub.game.personage.personagetype.PersonageType;
import org.geekhub.game.personage.personagetype.PersonageTypeInfo;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PersonageTypeInfoMapperTest {

    private PersonageTypeInfoMapper personageTypeInfoMapper = new PersonageTypeInfoMapper();

    @Test
    public void Should_Return_PersonageTypeInfoDto_When_ot_Type_Info_Dto() {
        PersonageTypeInfo personageTypeInfo = new PersonageTypeInfo();
        personageTypeInfo.setHealth(2);
        personageTypeInfo.setHunger(3);
        personageTypeInfo.setMentalHealth(4);
        personageTypeInfo.setDescription("dsd");
        personageTypeInfo.setDexterity(1);
        personageTypeInfo.setDefence(2);
        personageTypeInfo.setStrength(3);
        personageTypeInfo.setType(PersonageType.ROGUE);

        PersonageTypeInfoDto dto = personageTypeInfoMapper.toTypeInfoDto(personageTypeInfo);

        assertEquals(personageTypeInfo.getHealth(), dto.getHealth());
        assertEquals(personageTypeInfo.getDescription(), dto.getDescription());
        assertEquals(personageTypeInfo.getDexterity(), dto.getDexterity());
        assertEquals(personageTypeInfo.getDefence(), dto.getDefence());
        assertEquals(personageTypeInfo.getStrength(), dto.getStrength());
        assertEquals(personageTypeInfo.getType(), dto.getType());
    }


    @Test
    public void Should_Return_Mapped_Entity_Whem_To_Personage_Entity() {
        PersonageTypeInfo personageTypeInfo = new PersonageTypeInfo();
        personageTypeInfo.setHealth(2);
        personageTypeInfo.setHunger(3);
        personageTypeInfo.setMentalHealth(4);
        personageTypeInfo.setDescription("dsd");
        personageTypeInfo.setDexterity(1);
        personageTypeInfo.setDefence(2);
        personageTypeInfo.setStrength(3);
        personageTypeInfo.setType(PersonageType.ROGUE);

        Personage personage = personageTypeInfoMapper.toPersonageEntity(personageTypeInfo);

        assertEquals(personageTypeInfo.getHealth(), personage.getHealth());
        assertEquals(personageTypeInfo.getDescription(), personage.getDescription());
        assertEquals(personageTypeInfo.getDexterity(), personage.getDexterity());
        assertEquals(personageTypeInfo.getDefence(), personage.getDefence());
        assertEquals(personageTypeInfo.getStrength(), personage.getStrength());
        assertEquals(personageTypeInfo.getType(), personage.getType());
    }
}
