package org.geekhub.game.personage.mapper;

import org.geekhub.game.personage.Personage;
import org.geekhub.game.personage.dto.PersonageDto;
import org.geekhub.game.personage.dto.PersonageTypeInfoDto;
import org.springframework.stereotype.Component;

@Component
public class PersonageMapper {

    public PersonageDto toPersonageDto(Personage entity) {
        PersonageDto dto = new PersonageDto();

        dto.setType(entity.getType());
        dto.setHealth(entity.getHealth());
        dto.setMentalHealth(entity.getMentalHealth());
        dto.setDefence(entity.getDefence());
        dto.setStrength(entity.getStrength());
        dto.setDexterity(entity.getDexterity());
        dto.setActionCount(entity.getActionCount());
        dto.setHunger(entity.getHunger());
        dto.setLevel(entity.getLevel());

        return dto;
    }

}
