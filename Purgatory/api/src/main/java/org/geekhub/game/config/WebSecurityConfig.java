package org.geekhub.game.config;

import org.geekhub.game.config.security.AuthenticateUserProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private DataSource dataSource;
    private BCryptPasswordEncoder passwordEncoder;
    private AuthenticateUserProvider authProvider;

    public WebSecurityConfig(DataSource dataSource,
                             BCryptPasswordEncoder passwordEncoder,
                             AuthenticateUserProvider authProvider) {
        this.dataSource = dataSource;
        this.passwordEncoder = passwordEncoder;
        this.authProvider = authProvider;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/swagger-ui.html#", "/swagger-resources");
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable();

        http.csrf().disable().authorizeRequests()

                .antMatchers("/login").permitAll()
                .antMatchers("/registration").permitAll()
                .antMatchers("/swagger-ui.html#", "/swagger-resources").permitAll()

                .antMatchers("/game/**").hasAuthority("ROLE_USER")
                .antMatchers("/admin").hasAuthority("ROLE_ADMIN")
                .antMatchers("/admin/*").hasAuthority("ROLE_ADMIN")
                .antMatchers("/admin/users/*").hasAuthority("ROLE_ADMIN")
                .antMatchers("/admin/users/{id}").hasAuthority("ROLE_ADMIN")
                .antMatchers("/admin/users/{id}/*").hasAuthority("ROLE_ADMIN")

                .antMatchers("/admin/users/{id}/access/*").hasAuthority("ROLE_SUPER_ADMIN")
//                .anyRequest().authenticated()
                .and()
                .formLogin().defaultSuccessUrl("/users/me")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login").permitAll()
                .and()
                .rememberMe()
                .tokenValiditySeconds(604800);
    }


    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(authProvider);
        auth.jdbcAuthentication()
                .passwordEncoder(passwordEncoder)
                .dataSource(dataSource)
                .usersByUsernameQuery("select email, password, is_active from users where email=?")
                .authoritiesByUsernameQuery("select email, authority from users where email = ?");

    }

    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_SUPER_ADMIN > ROLE_ADMIN > ROLE_USER > ROLE_GUEST");
        return roleHierarchy;
    }

}