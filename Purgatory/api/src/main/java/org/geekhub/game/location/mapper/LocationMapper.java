package org.geekhub.game.location.mapper;

import org.geekhub.game.location.Location;
import org.geekhub.game.location.dto.LocationInfoDto;
import org.springframework.stereotype.Component;

@Component
public class LocationMapper {

       public LocationInfoDto toLocationInfo(Location location) {
        LocationInfoDto locationInfoDto = new LocationInfoDto();
        locationInfoDto.setDescription(location.getDescription());
        locationInfoDto.setName(location.getName());

        return locationInfoDto;
    }

}
