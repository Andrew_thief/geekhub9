package org.geekhub.game.location;

import org.geekhub.game.exception.PersonageNotFoundException;
import org.geekhub.game.gameaction.GameAction;
import org.geekhub.game.gameaction.GameActionResolver;
import org.geekhub.game.location.dto.GameActionInfoDto;
import org.geekhub.game.location.dto.LocationInfoDto;
import org.geekhub.game.location.mapper.GameActionMapper;
import org.geekhub.game.location.mapper.LocationMapper;
import org.geekhub.game.personage.Personage;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUsersService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/game")
public class LocationController {


    private LocationService locationService;
    private LocationMapper locationMapper;
    private GameActionResolver gameActionResolver;
    private ApplicationUsersService usersService;
    private GameActionMapper gameActionMapper;

    public LocationController(LocationService locationService, LocationMapper locationMapper, GameActionResolver gameActionResolver, ApplicationUsersService usersService, GameActionMapper gameActionMapper) {
        this.locationService = locationService;
        this.locationMapper = locationMapper;
        this.gameActionResolver = gameActionResolver;
        this.usersService = usersService;
        this.gameActionMapper = gameActionMapper;
    }

    @GetMapping("/location/info")
    public LocationInfoDto getLocationInfoByLevel(@AuthenticationPrincipal ApplicationUser applicationUser) {
        Personage personage = applicationUser.getPersonage();
        if (personage == null) {
            throw new PersonageNotFoundException("Before loading Location create new Personage");
        }

        Location location = locationService.getLocationByLevel(personage.getLevel());
        return locationMapper.toLocationInfo(location);
    }

    @PostMapping("/location/action")
    public GameActionInfoDto doAction(@AuthenticationPrincipal ApplicationUser applicationUser) {
        Personage personage = applicationUser.getPersonage();
        String actionDescription;

        if (!gameActionResolver.isAlive(personage)) {
            applicationUser.setPersonage(null);
            actionDescription = "Personage is dead. Please Create new personage and start new game";
        } else {
            Location location = locationService.getLocationByLevel(personage.getLevel());
            List<GameAction> actions = location.getGameAction();
            GameAction action = gameActionResolver.getGameAction(actions);

            applicationUser.setPersonage(gameActionResolver.changeCharacteristics(action, personage));
            actionDescription = action.getDescription();
        }
        usersService.saveUsersInfo(applicationUser);
        return gameActionMapper.toGameActionInfo(actionDescription, personage.getHealth(),
                personage.getMentalHealth(),
                personage.getHunger(), gameActionResolver.isAlive(personage));
    }

}
