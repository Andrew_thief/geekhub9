package org.geekhub.game.personage.dto;

import org.geekhub.game.personage.personagetype.PersonageType;

public class PersonageTypeInfoDto {

    private PersonageType type;
    private String description;
    private int health;
    private int strength;
    private int dexterity;
    private int defence;
    private int actionCount;

    public PersonageType getType() {
        return type;
    }

    public void setType(PersonageType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public int getActionCount() {
        return actionCount;
    }

    public void setActionCount(int actionCount) {
        this.actionCount = actionCount;
    }
}
