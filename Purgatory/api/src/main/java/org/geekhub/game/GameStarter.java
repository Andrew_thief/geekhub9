package org.geekhub.game;

import org.geekhub.game.config.GameConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameStarter extends SpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(GameConfig.class, args);
    }
}

