package org.geekhub.game.config.security;

import org.geekhub.game.exception.PasswordNotMuchException;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUsersService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticateUserProvider implements AuthenticationProvider {


    private ApplicationUsersService userService;
    private PasswordEncoder passwordEncoder;

    public AuthenticateUserProvider(ApplicationUsersService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();

        ApplicationUser user = userService.getByEmail(email);

        if ( passwordEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new PasswordNotMuchException("Password is not correct");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

