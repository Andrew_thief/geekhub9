package org.geekhub.game.personage.mapper;

import org.geekhub.game.personage.Personage;
import org.geekhub.game.personage.dto.PersonageTypeInfoDto;
import org.geekhub.game.personage.personagetype.PersonageTypeInfo;
import org.springframework.stereotype.Component;

@Component
public class PersonageTypeInfoMapper {

    public PersonageTypeInfoDto toTypeInfoDto(PersonageTypeInfo entity) {
        PersonageTypeInfoDto dto = new PersonageTypeInfoDto();

        dto.setType(entity.getType());
        dto.setDescription(entity.getDescription());
        dto.setHealth(entity.getHealth());
        dto.setDefence(entity.getDefence());
        dto.setStrength(entity.getStrength());
        dto.setDexterity(entity.getDexterity());
        dto.setActionCount(entity.getActionCount());

        return dto;
    }

    public Personage toPersonageEntity(PersonageTypeInfo infoEntity) {
        Personage personageEntity = new Personage();

        personageEntity.setType(infoEntity.getType());
        personageEntity.setDescription(infoEntity.getDescription());
        personageEntity.setHunger(infoEntity.getHunger());
        personageEntity.setMentalHealth(infoEntity.getMentalHealth());
        personageEntity.setHealth(infoEntity.getHealth());
        personageEntity.setStrength(infoEntity.getStrength());
        personageEntity.setDexterity(infoEntity.getDexterity());
        personageEntity.setDefence(infoEntity.getDefence());
        personageEntity.setActionCount(infoEntity.getActionCount());

        return personageEntity;
    }
}
