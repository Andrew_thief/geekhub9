package org.geekhub.game.user;


import org.geekhub.game.exception.PasswordNotMuchException;
import org.geekhub.game.user.dto.ProfileDto;
import org.geekhub.game.user.mapper.UserMapper;
import org.slf4j.Logger;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final ApplicationUsersService applicationUsersService;
    private final UserMapper mapper;
    private final BCryptPasswordEncoder passwordEncoder;
    private static final Logger log = getLogger(UsersController.class);

    public UsersController(ApplicationUsersService applicationUsersService, UserMapper mapper, BCryptPasswordEncoder passwordEncoder) {
        this.applicationUsersService = applicationUsersService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/me")
    public ProfileDto getUsersProfile(@AuthenticationPrincipal ApplicationUser user) {
        return mapper.toProfile(user);
    }

    @PutMapping("/me/first-name")
    public void changeFirstName(@AuthenticationPrincipal ApplicationUser user,
                                @RequestParam String firstName) {
        user.setFirstName(firstName);
        applicationUsersService.saveUsersInfo(user);
    }

    @PutMapping("/me/last-name")
    public void changeLastName(@AuthenticationPrincipal ApplicationUser user,
                               @RequestParam String lastName) {
        user.setLastName(lastName);
        applicationUsersService.saveUsersInfo(user);
    }

    @PostMapping("/me/email")
    public void changeEmail(@AuthenticationPrincipal ApplicationUser user,
                            @RequestParam String email) {
        applicationUsersService.checkEmailUsage(email);

        user.setEmail(email);
        applicationUsersService.saveUsersInfo(user);
    }

    @PutMapping("/me/password")
    public void changePassword(@AuthenticationPrincipal ApplicationUser user,
                               @RequestParam String pass1,
                               @RequestParam String pass2) {
        if (pass1.equals(pass2)) {
            String password = passwordEncoder.encode(pass1);
            user.setPassword(password);
            applicationUsersService.saveUsersInfo(user);
        } else {
            throw new PasswordNotMuchException("Passwords does`t much. Please try again.");
        }
    }

    @DeleteMapping("/me")
    public void delete(@AuthenticationPrincipal ApplicationUser user,
                       HttpServletRequest request) {
        user.setActive(false);

        applicationUsersService.saveUsersInfo(user);
        try {
            request.logout();
        } catch (ServletException e) {
            log.error(e.getMessage(), e);
        }
    }
}