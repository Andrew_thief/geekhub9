package org.geekhub.game.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);


    @ExceptionHandler(IllegalRightException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ModelAndView handle(IllegalRightException ex) {
        log.error(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(PasswordNotMuchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handle(PasswordNotMuchException ex) {
        log.warn(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(EmailInUseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handle(EmailInUseException ex) {
        log.warn(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(IllegalOperationException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ModelAndView handle(IllegalOperationException ex) {
        log.error(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(PersonageIsDeadException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handle(PersonageIsDeadException ex) {
        log.error(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }


    @ExceptionHandler(LocationNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handle(LocationNotFoundException ex) {
        log.error(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ExceptionHandler(PersonageNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handle(PersonageNotFoundException ex) {
        log.error(ex.getMessage(), ex);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserNotFoundException.class)
    public ModelAndView handle(UserNotFoundException ex) {
        log.warn(ex.getMessage());

        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMessage", ex.getMessage());
        return mav;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(Exception.class)
    public ModelAndView handle(Exception ex) {
        ModelAndView mav = new ModelAndView("error");

        if (ex.getMessage() == null) {
            mav.addObject("errorMessage", "Error!User not authenticated");
        } else {
            log.warn(ex.getMessage());
            mav.addObject("errorMessage", ex.getMessage());
        }
        return mav;
    }

}
