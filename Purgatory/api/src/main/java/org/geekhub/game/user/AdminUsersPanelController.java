package org.geekhub.game.user;

import org.geekhub.game.exception.PasswordNotMuchException;
import org.geekhub.game.user.dto.CreatigUserDto;
import org.geekhub.game.user.dto.WidenedProfileDto;
import org.geekhub.game.user.mapper.UserMapper;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminUsersPanelController {

    private ApplicationUsersService usersService;
    private UserMapper mapper;
    private final BCryptPasswordEncoder passwordEncoder;

    public AdminUsersPanelController(ApplicationUsersService usersService, UserMapper mapper, BCryptPasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/users")
    public List<ApplicationUser> getAllActiveUsers() {
        return usersService.getByActive(true);
    }

    @GetMapping("/users/disabled")
    public List<ApplicationUser> getAllDisabledUsers() {
        return usersService.getByActive(false);
    }

    @GetMapping("/users/{id}")
    public WidenedProfileDto getUserInfoById(@PathVariable long id) {
        ApplicationUser user = usersService.getById(id);

        return mapper.toWidenedProfile(user);
    }

    @PutMapping("/users/{id}/disable")
    public void disableUserById(@PathVariable long id,
                                @AuthenticationPrincipal ApplicationUser admin) {

        ApplicationUser user = usersService.getById(id);
        Authority adminAuthority = admin.getAuthority();

        usersService.setUsersActivity(user, false, adminAuthority);
    }

    @PutMapping("/users/{id}/activate")
    public void activateUserById(@PathVariable long id,
                                 @AuthenticationPrincipal ApplicationUser admin) {

        ApplicationUser user = usersService.getById(id);
        Authority adminAuthority = admin.getAuthority();

        usersService.setUsersActivity(user, true, adminAuthority);
    }

    @PutMapping("/users/{id}/first-name")
    public void changeUsersFirstName(@PathVariable long id,
                                     @RequestParam String firstName) {
        ApplicationUser user = usersService.getById(id);

        user.setFirstName(firstName);
        usersService.saveUsersInfo(user);
    }

    @PutMapping("/users/{id}/last-name")
    public void changeUsersLastName(@PathVariable long id,
                                    @RequestParam String lastName) {
        ApplicationUser user = usersService.getById(id);

        user.setLastName(lastName);
        usersService.saveUsersInfo(user);
    }

    @PostMapping("/users/{id}/email")
    public void changeUsersEmail(@PathVariable long id,
                                 @RequestParam String email) {
        ApplicationUser user = usersService.getById(id);
        usersService.checkEmailUsage(email);

        user.setEmail(email);
        usersService.saveUsersInfo(user);
    }

    @PutMapping("/users/{id}/password")
    public void changeUsersPassword(@PathVariable long id,
                                    @RequestParam String pass1,
                                    @RequestParam String pass2) {
        ApplicationUser user = usersService.getById(id);

        if (pass1.equals(pass2)) {
            String password = passwordEncoder.encode(pass1);
            user.setPassword(password);
            usersService.saveUsersInfo(user);
        } else {
            throw new PasswordNotMuchException("Passwords does`t much. Please try again.");
        }
    }


    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable long id,
                           @AuthenticationPrincipal ApplicationUser admin) {
        ApplicationUser user = usersService.getById(id);
        Authority adminAuthority = admin.getAuthority();

        usersService.deleteUser(user, adminAuthority);
    }

    @PostMapping("/users/add")
    public void createNewUser(CreatigUserDto creatigUserDto) {
        String email = creatigUserDto.getEmail();
        usersService.checkEmailUsage(email);

        String firstPhrase = creatigUserDto.getPass1();
        String secondPhrase = creatigUserDto.getPass2();

        if (firstPhrase.equals(secondPhrase)) {
            creatigUserDto.setPass1(passwordEncoder.encode(secondPhrase));
        } else {
            throw new PasswordNotMuchException("Passwords does`t much. Please try again.");
        }

        ApplicationUser newUser = mapper.toEntity(creatigUserDto);
        newUser.setAuthority(Authority.ROLE_USER);

        usersService.saveNewUser(newUser);
    }

    @PutMapping("/users/{id}/access/set-admin")
    public void setAdminAccessLevel(@PathVariable long id) {
        ApplicationUser user = usersService.getById(id);

        usersService.setAdminAuthority(user);
    }

    @PutMapping("/users/{id}/access/set-user")
    public void setUserAccessLevel(@PathVariable long id) {
        ApplicationUser user = usersService.getById(id);

        usersService.setUserAuthority(user);
    }

}