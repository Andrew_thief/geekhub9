package org.geekhub.game.user;

import org.geekhub.game.exception.PasswordNotMuchException;
import org.geekhub.game.user.dto.CreatigUserDto;
import org.geekhub.game.user.mapper.UserMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private final ApplicationUsersService usersService;
    private final UserMapper mapper;
    private final BCryptPasswordEncoder passwordEncoder;

    public RegistrationController(ApplicationUsersService usersService,
                                  UserMapper mapper,
                                  BCryptPasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public void registerNewUser(CreatigUserDto creatigUserDto, HttpServletRequest request) throws ServletException {
        String email = creatigUserDto.getEmail();
        usersService.checkEmailUsage(email);

        String firstPhrase = creatigUserDto.getPass1();
        String secondPhrase = creatigUserDto.getPass2();

        if (firstPhrase.equals(secondPhrase)) {
            creatigUserDto.setPass1(passwordEncoder.encode(secondPhrase));

            ApplicationUser user = mapper.toEntity(creatigUserDto);
            user.setAuthority(Authority.ROLE_USER);
            user.setActive(true);
            usersService.saveNewUser(user);
            request.login(email, firstPhrase);
        } else {
            throw new PasswordNotMuchException("Passwords does`t much. Please try again.");
        }
    }

}
