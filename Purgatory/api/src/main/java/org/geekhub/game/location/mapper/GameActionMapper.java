package org.geekhub.game.location.mapper;

import org.geekhub.game.location.dto.GameActionInfoDto;
import org.springframework.stereotype.Component;

@Component
public class GameActionMapper {


    public GameActionInfoDto toGameActionInfo(String description, int health, int mentalHealth, int hunger, boolean isAlive) {
        GameActionInfoDto object = new GameActionInfoDto();

        object.setDescription(description);
        object.setHunger(hunger);
        object.setMentalHealth(mentalHealth);
        object.setHealth(health);
        object.setAlive(isAlive);
        return object;
    }

}