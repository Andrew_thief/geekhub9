package org.geekhub.game.location.dto;

public class GameActionInfoDto {

    private String description;
    private boolean isAlive;
    private int health;
    private int mentalHealth;
    private int hunger;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMentalHealth() {
        return mentalHealth;
    }

    public void setMentalHealth(int mentalHealth) {
        this.mentalHealth = mentalHealth;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;

    }


    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
