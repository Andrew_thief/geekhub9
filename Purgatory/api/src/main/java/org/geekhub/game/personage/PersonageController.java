package org.geekhub.game.personage;

import org.geekhub.game.exception.PersonageNotFoundException;
import org.geekhub.game.personage.dto.PersonageDto;
import org.geekhub.game.personage.dto.PersonageTypeInfoDto;
import org.geekhub.game.personage.mapper.PersonageMapper;
import org.geekhub.game.personage.mapper.PersonageTypeInfoMapper;
import org.geekhub.game.personage.personagetype.PersonageType;
import org.geekhub.game.personage.personagetype.PersonageTypeInfo;
import org.geekhub.game.personage.personagetype.PersonageTypeService;
import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.ApplicationUsersService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/game/personages")
public class PersonageController {

    private PersonageTypeService personageTypeService;
    private ApplicationUsersService usersService;
    private PersonageTypeInfoMapper personageTypeInfoMapper;
    private PersonageMapper personageMapper;

    public PersonageController(PersonageTypeService personageTypeService,
                               ApplicationUsersService usersService,
                               PersonageTypeInfoMapper personageTypeInfoMapper,
                               PersonageMapper personageMapper) {
        this.personageTypeService = personageTypeService;
        this.usersService = usersService;
        this.personageTypeInfoMapper = personageTypeInfoMapper;
        this.personageMapper = personageMapper;
    }

    @GetMapping("/type")
    public PersonageTypeInfoDto getPersonageTypesInfo(@RequestParam PersonageType type) {
        PersonageTypeInfo personageTypeInfo = personageTypeService.getPersonageInfoByType(type);
        return personageTypeInfoMapper.toTypeInfoDto(personageTypeInfo);

    }

    @PutMapping("/my")
    public void createNewPersonage(@AuthenticationPrincipal ApplicationUser user, @RequestParam PersonageType type) {
        PersonageTypeInfo info = personageTypeService.getPersonageInfoByType(type);
        Personage newPersonage = personageTypeInfoMapper.toPersonageEntity(info);
        newPersonage.setLevel(1);
        newPersonage.setFighting(false);

        user.setPersonage(newPersonage);
        usersService.saveUsersInfo(user);
    }

    @GetMapping("/my")
    public PersonageDto getMyPersonage(@AuthenticationPrincipal ApplicationUser user) {

        if (user.getPersonage() != null) {
            return personageMapper.toPersonageDto(user.getPersonage());
        } else {
            throw new PersonageNotFoundException("You don`t have a personage. To start a game please create new.");
        }
    }

}
