package org.geekhub.game.user.mapper;

import org.geekhub.game.user.ApplicationUser;
import org.geekhub.game.user.dto.CreatigUserDto;
import org.geekhub.game.user.dto.ProfileDto;
import org.geekhub.game.user.dto.WidenedProfileDto;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public ApplicationUser toEntity(CreatigUserDto object) {
        ApplicationUser user = new ApplicationUser();

        user.setEmail(object.getEmail());
        user.setFirstName(object.getFirstName());
        user.setLastName(object.getLastName());
        user.setPassword(object.getPass1());

        return user;
    }

    public ProfileDto toProfile(ApplicationUser user) {
        ProfileDto object = new ProfileDto();

        object.setEmail(user.getEmail());
        object.setFirstName(user.getFirstName());
        object.setLastName(user.getLastName());

        return object;
    }

    public WidenedProfileDto toWidenedProfile(ApplicationUser user) {
        WidenedProfileDto object = new WidenedProfileDto();

        object.setId(user.getId());
        object.setEmail(user.getEmail());
        object.setFirstName(user.getFirstName());
        object.setLastName(user.getLastName());
        object.setActive(user.isActive());
        object.setAuthority(user.getAuthority());
        return object;
    }


}
