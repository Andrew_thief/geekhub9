package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CaesarCodecTest {
    private CaesarCodec caesarCodec;

    @BeforeMethod
    public void setUp() {
        caesarCodec = new CaesarCodec(17);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Null() {
        caesarCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_Is_Empty() {
        caesarCodec.encode("");
    }

    @Test
    public void Should_Encode_When_Input_With_One_Symbol() {
        assertEquals(caesarCodec.encode("a"), "r");
    }

    @Test
    public void Should_Encode_When_Input_With_Multiple_Symbols() {
        assertEquals(caesarCodec.encode("Testing is testing ZZA"), "Kvjkzex zj kvjkzex QQR");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_With_Unexpected_Symbols() {
        caesarCodec.encode(", я!");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Null() {
        caesarCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_Is_Empty() {
        caesarCodec.decode("");
    }

    @Test
    public void Should_Decode_When_Input_With_One_Symbol() {
        assertEquals(caesarCodec.decode("r"), "a");
    }

    @Test
    public void Should_Decode_When_Input_With_Multiple_Symbols() {
        assertEquals(caesarCodec.decode("Kvjkzex zj kvjkzex QQR"), "Testing is testing ZZA");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_With_Unexpected_Symbols() {
        caesarCodec.decode(", я!");
    }
}