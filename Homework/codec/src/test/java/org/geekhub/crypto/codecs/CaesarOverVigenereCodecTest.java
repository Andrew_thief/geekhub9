package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CaesarOverVigenereCodecTest {
    private CaesarOverVigenereCodec caesarOverVigenereCodec;

    @BeforeMethod
    public void setUp()  {
        caesarOverVigenereCodec = new CaesarOverVigenereCodec(new VigenereCodec("geekhub"),  new CaesarCodec(17));
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Null() {
        caesarOverVigenereCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_Is_Empty() {
        caesarOverVigenereCodec.encode("");
    }

    @Test
    public void Should_Encode_When_Input_With_One_Symbol() {
        assertEquals(caesarOverVigenereCodec.encode("a"), "x");
    }

    @Test
    public void Should_Encode_When_Input_With_Multiple_Symbols() {
        assertEquals(caesarOverVigenereCodec.encode("Testing is testing ZZA"), "Qznugyy fn ofqeakb UAY");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_With_Unexpected_Symbols() {
        caesarOverVigenereCodec.encode(",!");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Null() {
        caesarOverVigenereCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_Is_Empty() {
        caesarOverVigenereCodec.decode("");
    }

    @Test
    public void Should_Decode_When_Input_With_One_Symbol() {
        assertEquals(caesarOverVigenereCodec.decode("x"), "a");
    }

    @Test
    public void Should_Decode_When_Input_With_Multiple_Symbols() {
        assertEquals(caesarOverVigenereCodec.decode("Qznugyy fn ofqeakb UAY"), "Testing is testing ZZA");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_With_Unexpected_Symbols() {
        caesarOverVigenereCodec.decode(", я!");
    }
}