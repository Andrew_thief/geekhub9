package org.geekhub.crypto.codecs;

import org.geekhub.crypto.codecs.translate.TranslateCodec;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class TranslateCodecTest {
    private TranslateCodec translateCodec;

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Null() {
        translateCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_Is_Empty() {
        translateCodec.encode("");
    }

    @Test
    public void Should_Encode_When_Input_With_One_Word() {
        assertEquals(translateCodec.encode("ability"), "здатність");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Null() {
        translateCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_Is_Empty() {
        translateCodec.decode("");
    }

    @Test
    public void Should_Decode_When_Input_With_One_Word() {
        assertEquals(translateCodec.decode("здатність"), "ability");
    }

    @Test
    public void Should_Decode_When_Input_With_Multiple_Words() {
        assertEquals(
                translateCodec.decode("розвиток слово"), "development word");

    }
}