package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class VigenereCodecTest {
    private VigenereCodec vigenereCodec;

    @BeforeMethod
    public void setUp() {
        vigenereCodec = new VigenereCodec("geekhub");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Null() {
        vigenereCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Empty() {
        vigenereCodec.encode("");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Unexpected_Symbol(){
        vigenereCodec.encode("фів");
    }

    @Test
    public void Should_Encode_When_Input_With_One_Symbol() {
        assertEquals(vigenereCodec.encode("a"), "g");
    }

    @Test
    public void Should_Encode_When_Input_With_Multiple_Symbols() {
        assertEquals(vigenereCodec.encode("Testing is testing ZZA"), "Ziwdphh ow xoznjtk DJH");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Null() {
        vigenereCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_Is_Empty() {
        vigenereCodec.decode("");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Unexpected_Symbol(){
        vigenereCodec.decode("фів");
    }

    @Test
    public void Should_Decode_When_Input_With_One_Symbol() {
        assertEquals(vigenereCodec.decode("g"), "a");
    }

    @Test
    public void Should_Decode_When_Input_With_Multiple_Symbols() {
        assertEquals(vigenereCodec.decode("Ziwdphh ow xoznjtk DJ"), "Testing is testing ZZ");
    }

}