package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


public class MorseCodecTest {

    private MorseCodec morseCodec = new MorseCodec();

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_And_Throw_Exception_When_Input_Is_Null() {
        morseCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_Is_Empty() {
        morseCodec.encode("");
    }

    @Test
    public void Should_Encode_When_Input_With_One_Symbol() {
        assertEquals(morseCodec.encode("a"), ".-/");
    }

    @Test
    public void Should_Encode_When_Input_With_Multiple_Symbols() {
        assertEquals(
                morseCodec.encode("Testing is testing ZZA"),
                "-/./.../-/../-./--./......./../.../......./-/./.../-/../-./--./......./--../--../.-/");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Encode_When_Input_With_Unexpected_Symbols() {
        morseCodec.encode(", я!");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_And_Throw_Exception_When_Input_Is_Null() {
        morseCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_Is_Empty() {
        morseCodec.decode("");
    }

    @Test
    public void Should_Decode_When_Input_With_One_Symbol() {
        assertEquals(morseCodec.decode(".-/"), "A");
    }

    @Test
    public void Should_Decode_When_Input_With_Multiple_Symbols() {
        assertEquals(
                morseCodec.decode("-/./.../-/../-./--./......./../.../......./-/./.../-/../-./--./......./--../--../.-/"),
                "TESTING IS TESTING ZZA");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void Should_Not_Decode_When_Input_With_Unexpected_Symbols() {
        morseCodec.decode(", я!");
    }
}