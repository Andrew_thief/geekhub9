package org.geekhub.crypto.codecs;

public interface Decoder {
    String decode(String input);
}
