package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ConfigurationProperties(prefix = "morse")
public class MorseCodec implements Encoder, Decoder {
    private static final String SYMBOL_DELIMITER = "/";

    private final Map<String, String> cryptoMorse = new HashMap<>();

    @Override
    public String encode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot encode empty value.", MorseCodec.class);
        }
        String[] inputCharacters = input.toUpperCase().split("");
        StringBuilder result = new StringBuilder();
        for (String inputChar : inputCharacters) {
            result.append(getResultChar(inputChar));
        }
        return result.toString();
    }

    @Override
    public String decode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot decode empty value.", MorseCodec.class);
        }
        String[] inputCharacters = input.toLowerCase().split(SYMBOL_DELIMITER);
        StringBuilder result = new StringBuilder();
        for (String codeMorse : inputCharacters) {
            result.append(getResultChar(codeMorse));
        }
        return result.toString();
    }

    private String getResultChar(String inputChar) {
        if (cryptoMorse.containsKey(inputChar)) {
            return cryptoMorse.get(inputChar) + SYMBOL_DELIMITER;
        } else {
            throw new IllegalInputException("Cannot resolve symbol [" + inputChar + "].", MorseCodec.class);
        }
    }
}