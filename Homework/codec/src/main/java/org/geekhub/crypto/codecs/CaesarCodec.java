package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.function.IntBinaryOperator;

import static org.geekhub.crypto.codecs.Const.*;

@Component
public class CaesarCodec implements Encoder, Decoder {

    private int shift;

    public CaesarCodec(@Value("${Caesar.shift}") int shift) {
        this.shift = shift;
    }

    @Override
    public String encode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot encode empty value.", CaesarCodec.class);
        }
        char[] inputCharacters = input.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            result.append(doAction(inputCharacters[i], CaesarCodec::encodeWithCase));
        }
        return result.toString();
    }

    @Override
    public String decode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot decode empty value.", CaesarCodec.class);
        }
        char[] inputCharacters = input.toCharArray();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            result.append(doAction(inputCharacters[i], CaesarCodec::decodeWithCase));
        }
        return result.toString();
    }

    private char doAction(char input, IntBinaryOperator action) {
        if (input == WORDS_DELIMITER) {
            return input;
        }

        for (int j = 0; j < ALPHABET_WITH_CASES.length; j++) {
            if (input == ALPHABET_WITH_CASES[j]) {
                return ALPHABET_WITH_CASES[action.applyAsInt(j, shift)];
            }
        }

        throw new IllegalInputException("Cannot resolve symbol [" + input + "] in ", CaesarCodec.class);
    }

    private static int encodeWithCase(int iterator, int shiftNum) {
        if (iterator <= ALPHABET_LENGTH - 1) {
            return (iterator + shiftNum) % ALPHABET_LENGTH;
        } else {
            return (iterator + shiftNum) % ALPHABET_LENGTH + ALPHABET_LENGTH;
        }
    }

    private static int decodeWithCase(int iterator, int shiftNum) {
        if (iterator >= ALPHABET_LENGTH) {
            return decodeChar(iterator, shiftNum) + ALPHABET_LENGTH;
        } else {
            return decodeChar(iterator, shiftNum);
        }
    }

    private static int decodeChar(int iterator, int shiftNum) {
        return (iterator - shiftNum + ALPHABET_LENGTH) % ALPHABET_LENGTH;
    }
}