package org.geekhub.crypto.codecs;

public interface Encoder {
    String encode(String input);
}
