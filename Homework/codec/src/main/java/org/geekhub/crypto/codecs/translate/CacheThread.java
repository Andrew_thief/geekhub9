package org.geekhub.crypto.codecs.translate;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class CacheThread implements Runnable {

    private TranslateCodec translateCodec;
    private List<String> words;
    private ConcurrentMap<String, String> cache;

    public CacheThread(TranslateCodec translateCodec, List<String> words, ConcurrentMap<String, String> cache) {
        this.translateCodec = translateCodec;
        this.words = words;
        this.cache = cache;
    }

    @Override
    public void run() {
        for (String word: words) {
            cache.put(word, translateCodec.encode(word));
            cache.put(translateCodec.decode(word), word);
        }
    }


}
