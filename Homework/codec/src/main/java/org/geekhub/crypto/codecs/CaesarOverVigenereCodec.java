package org.geekhub.crypto.codecs;

import org.springframework.stereotype.Component;

@Component
public class CaesarOverVigenereCodec implements Encoder, Decoder {
    private final Encoder vigenereEncoder;
    private final Decoder vigenereDecoder;
    private final Encoder caesarEncoder;
    private final Decoder caesarDecoder;

    public CaesarOverVigenereCodec(VigenereCodec vigenereCodec, CaesarCodec caesarCodec) {
        vigenereEncoder = vigenereCodec;
        vigenereDecoder = vigenereCodec;
        caesarEncoder = caesarCodec;
        caesarDecoder = caesarCodec;
    }

    @Override
    public String encode(String input) {
        return caesarEncoder.encode(vigenereEncoder.encode(input));
    }

    @Override
    public String decode(String input) {
        return vigenereDecoder.decode(caesarDecoder.decode(input));
    }
}
