package org.geekhub.crypto.exceptions;

import org.geekhub.crypto.codecs.Encoder;

public class IllegalInputException extends RuntimeException {
    public IllegalInputException(String message, Class<? extends Encoder> codec) {
        super(message + " Exception in codec: " + codec.getSimpleName());
    }
}
