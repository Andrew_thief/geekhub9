package org.geekhub.crypto.codecs;

public class Const {

    private Const() {
    }

    static final char[] ALPHABET_WITH_CASES =
            {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                    'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',

                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                    'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public static final char WORDS_DELIMITER = ' ';

    static final int ALPHABET_LENGTH = 26;

}
