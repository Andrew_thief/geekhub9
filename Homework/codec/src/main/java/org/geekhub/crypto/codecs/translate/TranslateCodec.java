package org.geekhub.crypto.codecs.translate;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.geekhub.crypto.codecs.Decoder;
import org.geekhub.crypto.codecs.Encoder;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.*;

@Component
public class TranslateCodec implements Encoder, Decoder {

    private static final String LANGUAGE_CODE_UK = "uk";
    private static final String LANGUAGE_CODE_EN = "en";

    private static final int ONE_HOUR = 1;
    private static final int WORKERS_NUM = 10;

    private TranslateOffline translateOffline;
    private TranslateRepository translateRepository;
    private ConcurrentMap<String, String> temporaryCache = new ConcurrentHashMap<>(WORKERS_NUM);

    public TranslateCodec(TranslateOffline translateOffline,
                          TranslateRepository translateRepository) {
        this.translateOffline = translateOffline;
        this.translateRepository = translateRepository;
    }

    @Override
    public String encode(String input) {
        checkInput(input);
        putInCache();

        if (temporaryCache.containsKey(input)) {
            return temporaryCache.get(input);
        } else {
            return translateInput(input, LANGUAGE_CODE_EN, LANGUAGE_CODE_UK);
        }
    }

    @Override
    public String decode(String input) {
        checkInput(input);
        putInCache();

        if (temporaryCache.containsKey(input)) {
            return temporaryCache.get(input);
        } else {
            return translateInput(input, LANGUAGE_CODE_UK, LANGUAGE_CODE_EN);
        }
    }

    private String translateInput(String input, String fromLang, String toLang) {
        String query = "key=AIzaSyB2HijQLlsmI1udH9ARl45oC5eAj4XfjTw"
                + "&source=" + fromLang
                + "&target=" + toLang
                + "&q=" + URLEncoder.encode(input, StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://www.googleapis.com/language/translate/v2?" + query))
                .header("Referer", "https://www.daytranslations.com/free-translation-online/")
                .GET()
                .build();

        try {
            String responseJson = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString())
                    .body();
            return getDataFromJson(responseJson);

        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            return translateOffline.translate(input);
        }
    }

    private String getDataFromJson(String json) {
        return new Gson().fromJson(json, JsonObject.class).getAsJsonObject("data")
                .getAsJsonArray("translations")
                .get(0)
                .getAsJsonObject()
                .get("translatedText").getAsString();
    }

    private void checkInput(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot encode empty value.", TranslateCodec.class);
        }
    }

    private void putInCache() {
        List<String> words = translateRepository.getMostPopularWords();
        ScheduledExecutorService service = null;

        try {
            service = Executors.newScheduledThreadPool(WORKERS_NUM);
            service.scheduleWithFixedDelay(
                    new CacheThread((this), words, temporaryCache),
                    0, ONE_HOUR, TimeUnit.HOURS);
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }


}