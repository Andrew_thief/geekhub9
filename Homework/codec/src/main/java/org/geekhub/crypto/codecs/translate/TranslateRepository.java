package org.geekhub.crypto.codecs.translate;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TranslateRepository {

    private JdbcTemplate jdbcTemplate;

    public TranslateRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> getMostPopularWords() {
        return jdbcTemplate.query("select word from popular_en_word;",
                new WordMapping());
    }
}
