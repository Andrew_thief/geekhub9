package org.geekhub.crypto.codecs.translate;


import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

import static org.geekhub.crypto.codecs.Const.WORDS_DELIMITER;

@Component

public class TranslateOffline {

    private Map<String, String> cryptoDictionaryUkToEn;
    private Map<String, String> cryptoDictionaryEnToUk;

//    public TranslateOffline(@Value("#{${dictionary.cryptoDictionaryEnToUk}}")
//                                    Map<String, String> cryptoDictionaryUkToEn) {
//        this.cryptoDictionaryUkToEn = cryptoDictionaryUkToEn;
//    }

    public String translate(String input) {
        String[] words = input.toLowerCase().split(" ");
        StringBuilder result = new StringBuilder();

        for (String word : words) {
            result.append(getTranslateResult(word)).append(WORDS_DELIMITER);
        }
        return result.toString();
    }

    private String getTranslateResult(String input) {
        return cryptoDictionaryUkToEn.computeIfAbsent(input, unexpected -> {
            throw new IllegalInputException("Illegal input. Words [" + unexpected + "] are absent in UA/EN dictionary.",
                    TranslateCodec.class);
        });
    }
}
