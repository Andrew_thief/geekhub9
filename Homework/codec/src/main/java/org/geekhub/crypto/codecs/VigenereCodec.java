package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.function.IntBinaryOperator;

import static org.geekhub.crypto.codecs.Const.*;

@Component
public class VigenereCodec implements Encoder, Decoder {

    private String keyWord;

    public VigenereCodec(@Value("${Vigenere.keyword}") String keyword) {
        this.keyWord = keyword;
    }

    @Override
    public String encode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot encode empty value.", VigenereCodec.class);

        }
        char[] inputCharacters = input.toCharArray();
        StringBuilder result = new StringBuilder();
        char[] key = genereteKeyPhrase(input);
        for (int i = 0; i < input.length(); i++) {
            result.append(doModification(inputCharacters[i], key[i], VigenereCodec::encodeWithCase));
        }
        return result.toString();
    }

    @Override
    public String decode(String input) {
        if (input == null || input.isEmpty()) {
            throw new IllegalInputException("Input is empty! Cannot decode empty value.", VigenereCodec.class);
        }
        char[] inputCharacters = input.toCharArray();
        StringBuilder result = new StringBuilder();
        char[] key = genereteKeyPhrase(input);
        for (int i = 0; i < input.length(); i++) {
            result.append(doModification(inputCharacters[i], key[i], VigenereCodec::decodeWithCase));
        }
        return result.toString();
    }

    private char doModification(char input, char keyChar, IntBinaryOperator action) {
        if (input == WORDS_DELIMITER) {
            return input;
        }

        for (int j = 0; j < ALPHABET_WITH_CASES.length; j++) {
            if (input == ALPHABET_WITH_CASES[j]) {
                return ALPHABET_WITH_CASES[action.applyAsInt(j, getShift(keyChar))];
            }
        }
        throw new IllegalInputException("Cannot resolve symbol [" + input + "].", VigenereCodec.class);
    }

    private static int encodeWithCase(int iterator, int shift) {
        if (iterator <= ALPHABET_LENGTH - 1) {
            return (iterator + shift) % ALPHABET_LENGTH;
        } else {
            return (iterator + shift) % ALPHABET_LENGTH + ALPHABET_LENGTH;
        }
    }

    private static int decodeWithCase(int iterator, int shift) {
        if (iterator >= ALPHABET_LENGTH) {
            return decodedSymbol(iterator, shift) + ALPHABET_LENGTH;
        } else {
            return (iterator - shift + ALPHABET_LENGTH) % ALPHABET_LENGTH;
        }
    }

    private static int decodedSymbol(int iterator, int shift) {
        return (iterator - shift + ALPHABET_LENGTH) % ALPHABET_LENGTH;
    }

    private char[] genereteKeyPhrase(String inputPhrase) {
        char[] phrase = inputPhrase.toCharArray();
        char[] keyword = keyWord.toCharArray();

        for (int i = 0, j = 0; i < phrase.length; i++) {
            if (phrase[i] == WORDS_DELIMITER) {
                phrase[i] = WORDS_DELIMITER;
            } else {
                phrase[i] = keyword[j % keyWord.length()];
                j++;
            }
        }
        return phrase;
    }

    private int getShift(char keyChar) {
        int shiftNum = 0;

        for (int i = 0; i < ALPHABET_LENGTH; i++) {
            if (ALPHABET_WITH_CASES[i] == keyChar) {
                shiftNum = i;
            }
        }
        return shiftNum;
    }
}