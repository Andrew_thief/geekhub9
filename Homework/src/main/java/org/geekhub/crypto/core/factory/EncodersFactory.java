package org.geekhub.crypto.core.factory;

import org.geekhub.crypto.codecs.translate.TranslateCodec;
import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.codecs.*;
import org.geekhub.crypto.core.exceptions.WrongAlgorithmException;
import org.springframework.stereotype.Component;


@Component
public class EncodersFactory {

    private CaesarCodec caesarCodec;
    private VigenereCodec vigenereCodec;
    private TranslateCodec translateCodec;
    private CaesarOverVigenereCodec caesarOverVigenereCodec;
    private MorseCodec morseCodec;

    public EncodersFactory(CaesarCodec caesarCodec,
                           VigenereCodec vigenereCodec,
                           TranslateCodec translateCodec,
                           CaesarOverVigenereCodec caesarOverVigenereCodec,
                           MorseCodec morseCodec) {
        this.caesarCodec = caesarCodec;
        this.vigenereCodec = vigenereCodec;
        this.translateCodec = translateCodec;
        this.caesarOverVigenereCodec = caesarOverVigenereCodec;
        this.morseCodec = morseCodec;
    }

    public Encoder getEncoder(Algorithms algorithm) {
        if (algorithm == null) {
            throw new WrongAlgorithmException("Algorithm is empty or null!");
        }

        switch (algorithm) {
            case MORSE:
                return morseCodec;
            case VIGENERE:
                return vigenereCodec;
            case CAESAR:
                return caesarCodec;
            case TRANSLATE:
                return translateCodec;
            case CAESAR_OVER_VIGENERE:
                return caesarOverVigenereCodec;
            default:
                throw new WrongAlgorithmException("Algorithm [" + algorithm + "] not found!");
        }
    }


}
