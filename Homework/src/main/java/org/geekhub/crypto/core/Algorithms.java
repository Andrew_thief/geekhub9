package org.geekhub.crypto.core;

public enum Algorithms {
    CAESAR("Caesar cipher"),
    MORSE("Morse code"),
    VIGENERE("Vigenere cipher"),
    CAESAR_OVER_VIGENERE("Caesar over Vigenere"),
    TRANSLATE("Translate");

    private String algorithm;

    Algorithms(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getAlgorithm() {
        return algorithm;
    }
}
