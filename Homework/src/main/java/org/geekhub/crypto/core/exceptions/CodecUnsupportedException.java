package org.geekhub.crypto.core.exceptions;

public class CodecUnsupportedException extends RuntimeException {
    public CodecUnsupportedException(String message) {
        super(message);
    }
}
