package org.geekhub.crypto.core.exceptions;

public class OperationUnsupportedException extends RuntimeException {
    public OperationUnsupportedException(String message) {
        super(message);
    }
}
