package org.geekhub.crypto.core.history;

import java.util.List;

public interface HistoryPrinter {
    void print(List<HistoryRecord> records);
}
