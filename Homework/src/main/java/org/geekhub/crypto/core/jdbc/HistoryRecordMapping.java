package org.geekhub.crypto.core.jdbc;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Component
public class HistoryRecordMapping implements RowMapper<HistoryRecord> {

    @Override
    public HistoryRecord mapRow(ResultSet rs, int rownumber) throws SQLException {
        String op = rs.getString("operation");
        String alg = rs.getString("algorithm");
        String msg = rs.getString("message");
        String res = rs.getString("result");
        LocalDate date = rs.getDate("dt").toLocalDate();

        Algorithms algorithm;
        ContextOperations operation = ContextOperations.valueOf(op);

        if (alg != null || alg != "null") {
            algorithm = Algorithms.valueOf(alg);
            return new HistoryRecord(algorithm, operation, msg, res, date);
        } else {
            return new HistoryRecord(operation, date);
        }
    }

}
