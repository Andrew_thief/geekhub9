package org.geekhub.crypto.core.history;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.ui.console.ContextOperations;

import java.io.Serializable;
import java.time.LocalDate;

public class HistoryRecord implements Serializable {

    private Algorithms algorithmType;
    private ContextOperations operationType;
    private String usersMessage;
    private String operationResult;
    private LocalDate date;

    public HistoryRecord(ContextOperations operationType, LocalDate date) {
        this.operationType = operationType;
        this.date = date;
    }

    public HistoryRecord(Algorithms algorithmType, ContextOperations operationType,
                         String usersMessage, String operationResult, LocalDate date) {
        this.algorithmType = algorithmType;
        this.operationType = operationType;
        this.usersMessage = usersMessage;
        this.operationResult = operationResult;
        this.date = date;
    }

    public Algorithms getAlgorithmType() {
        return algorithmType;
    }

    public ContextOperations getOperationType() {
        return operationType;
    }

    public String getUsersMessage() {
        return usersMessage;
    }

    public String getOperationResult() {
        return operationResult;
    }

    public LocalDate getDate() {
        return date;
    }
}
