package org.geekhub.crypto.core.jdbc;

import org.geekhub.crypto.core.history.HistoryRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HistoryRecordRepository {

    private JdbcTemplate jdbcTemplate;
    private HistoryRecordMapping historyRecordMapping;

    @Autowired
    public HistoryRecordRepository(JdbcTemplate jdbcTemplate,
                                   HistoryRecordMapping historyRecordMapping) {
        this.jdbcTemplate = jdbcTemplate;
        this.historyRecordMapping = historyRecordMapping;
    }

    public List<HistoryRecord> getAllHistoryRecords() {
        return jdbcTemplate.query("select operation, algorithm, message,result,dt from history",
                historyRecordMapping);
    }


    public void saveRecordToDatabase(HistoryRecord historyRecord) {
        String operation = historyRecord.getOperationType().toString();
        String msg = historyRecord.getUsersMessage();
        String dt = historyRecord.getDate().toString();

        if (msg == null) {
            jdbcTemplate.update("insert into history (operation, dt) values (?,?)",
            operation, dt);
        } else {
            String alg = historyRecord.getAlgorithmType().toString();
            String result = historyRecord.getOperationResult();
            jdbcTemplate.update("insert into history (operation,algorithm,message,result, dt) values (?,?,?,?,?)",
            operation,alg, msg, result, dt);
        }
    }

    public void deleteLastRecordFromDatabase() {
        jdbcTemplate.execute("delete from history where id = MAX(id)");
    }

    public void deleteAllRecordsFromDatabase() {
        jdbcTemplate.execute("delete from history");
    }

}

