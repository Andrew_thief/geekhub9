package org.geekhub.crypto.core.exceptions;

public class EmptyHistoryException extends RuntimeException {
    public EmptyHistoryException(String message) {
        super(message);
    }
}
