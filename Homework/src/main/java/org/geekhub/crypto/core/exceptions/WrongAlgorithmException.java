package org.geekhub.crypto.core.exceptions;

public class WrongAlgorithmException extends RuntimeException {
    public WrongAlgorithmException(String message) {
        super(message);
    }
}
