package org.geekhub.crypto.core.analytics;

public enum CodecUsecase {
    ENCODING,
    DECODING
}
