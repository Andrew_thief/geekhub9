package org.geekhub.crypto.core.analytics;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.exceptions.EmptyHistoryException;
import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static org.geekhub.crypto.core.analytics.CodecUsecase.DECODING;
import static org.geekhub.crypto.core.analytics.CodecUsecase.ENCODING;
import static org.geekhub.crypto.ui.console.ContextOperations.DECODE;
import static org.geekhub.crypto.ui.console.ContextOperations.ENCODE;

@Component
public class CodingAudit {
    private final HistoryManager history;

    public CodingAudit(HistoryManager history) {
        this.history = history;
    }

    public Map<String, Integer> countEncodingInputs() {
        return prepareUsecaseRecords(ENCODING).stream()
                .flatMap(record -> Arrays.stream(record.getUsersMessage().split(" ")))
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.summingInt(e -> 1)
                ));
    }

    public Map<LocalDate, Long> countCodingsByDate(CodecUsecase codecUsecase) {
        return prepareUsecaseRecords(codecUsecase).stream()
                .map(HistoryRecord::getDate)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        counting()
                ));
    }

    public Algorithms findMostPopularCodec(CodecUsecase codecUsecase) {
        Optional<Map.Entry<Algorithms, Integer>> mostPopular = getMaxValue(countAlgorithms(codecUsecase));
        return mostPopular.map(Map.Entry::getKey)
                .orElseThrow(() -> new EmptyHistoryException("Can`t find most popular Codec." +
                        " History is empty. Please do some operation before use audit!"));
    }

    private Map<Algorithms, Integer> countAlgorithms(CodecUsecase codecUsecase) {
        return prepareUsecaseRecords(codecUsecase).stream()
                .map(HistoryRecord::getAlgorithmType)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.summingInt(e -> 1)
                ));
    }

    private Optional<Map.Entry<Algorithms, Integer>> getMaxValue(Map<Algorithms, Integer> countedAlgoithms) {
        return countedAlgoithms.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());
    }

    private List<HistoryRecord> prepareUsecaseRecords(CodecUsecase codecUsecase) {
        return history.getAllRecords().stream()
                .filter(record -> (codecUsecase == ENCODING && record.getOperationType() == ENCODE)
                        || (codecUsecase == DECODING && record.getOperationType() == DECODE))
                .collect(Collectors.toList());
    }
}