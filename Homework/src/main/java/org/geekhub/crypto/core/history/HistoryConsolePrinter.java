package org.geekhub.crypto.core.history;

import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HistoryConsolePrinter implements HistoryPrinter {

    @Override
    public void print(List<HistoryRecord> records) {

        int i = 1;
        System.out.println("User history: ");
        for (HistoryRecord record : records) {
            if (record.getOperationType().equals(ContextOperations.DECODE) ||
                    record.getOperationType().equals(ContextOperations.ENCODE)
            ) {
                System.out.println(i + ". [" + record.getDate() + "] "
                        + record.getOperationType().getOperation() + " : " + record.getAlgorithmType() + " "
                        + record.getUsersMessage() + " => " + record.getOperationResult());
                i++;
            } else {
                System.out.println(i + ". [" + record.getDate() + "] "
                        + record.getOperationType().getOperation());
                i++;
            }
        }
    }
}

