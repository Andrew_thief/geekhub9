package org.geekhub.crypto.core.history;

import org.geekhub.crypto.core.jdbc.HistoryRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HistoryManager {
    private HistoryRecordRepository databaseController;

    @Autowired
    public HistoryManager(HistoryRecordRepository databaseController) {
        this.databaseController = databaseController;
    }

    public void saveToHistory(HistoryRecord record) {
        databaseController.saveRecordToDatabase(record);
    }

    public List<HistoryRecord> getAllRecords() {
        return databaseController.getAllHistoryRecords();
    }

    public void deleteAllRecords() {
        databaseController.deleteAllRecordsFromDatabase();
    }

    public void removeLastRecord() {
        databaseController.deleteLastRecordFromDatabase();
    }
}
