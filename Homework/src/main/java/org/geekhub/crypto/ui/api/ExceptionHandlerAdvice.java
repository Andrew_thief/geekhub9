package org.geekhub.crypto.ui.api;

import org.geekhub.crypto.core.exceptions.CodecUnsupportedException;
import org.geekhub.crypto.core.exceptions.EmptyHistoryException;
import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(CodecUnsupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(CodecUnsupportedException ex) {
        return new ErrorDto(ex.getMessage());
    }

    @ExceptionHandler(OperationUnsupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(OperationUnsupportedException ex) {
        return new ErrorDto(ex.getMessage());
    }

    @ExceptionHandler(EmptyHistoryException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(EmptyHistoryException ex) {
        return new ErrorDto(ex.getMessage());
    }

    @ExceptionHandler(IllegalInputException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleIllegalInputException(IllegalInputException ex) {
        return new ErrorDto(ex.getMessage());
    }

}
