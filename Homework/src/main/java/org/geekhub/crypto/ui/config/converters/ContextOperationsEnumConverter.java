package org.geekhub.crypto.ui.config.converters;

import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.core.convert.converter.Converter;

public class ContextOperationsEnumConverter implements Converter<String, ContextOperations> {

    @Override
    public ContextOperations convert(String source) {
        try {
            return ContextOperations.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new OperationUnsupportedException("Operation [" + source + "] not found!");
        }
    }
}
