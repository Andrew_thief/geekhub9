package org.geekhub.crypto.ui.console;

import org.geekhub.crypto.core.analytics.CodecUsecase;
import org.geekhub.crypto.core.analytics.CodingAudit;
import org.geekhub.crypto.core.exceptions.EmptyHistoryException;
import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.geekhub.crypto.util.LoggingFacade;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Scanner;

import static org.geekhub.crypto.core.analytics.CodecUsecase.DECODING;
import static org.geekhub.crypto.core.analytics.CodecUsecase.ENCODING;
import static org.geekhub.crypto.ui.console.ContextOperations.*;

@Component
public class AuditMenu {
    private final LoggingFacade logger;
    private Scanner scanner;
    private CodingAudit audit;
    private HistoryManager historyManager;

    public AuditMenu(HistoryManager historyManager,
                     CodingAudit audit,
                     LoggingFacade logger) {
        this.scanner = new Scanner(System.in);
        this.historyManager = historyManager;
        this.audit = audit;
        this.logger = logger;
    }


    public void startAudit() {
        System.out.println("With what usecase you want audit (1)Encoding or (2)Decoding? ");
        String input = scanner.nextLine();
        if (input.toLowerCase().contains("encod") || input.contains("1")) {
            getAuditOperation(ENCODING);
        } else if (input.toLowerCase().contains("decod") || input.contains("2")) {
            getAuditOperation(DECODING);
        } else {
            throw new OperationUnsupportedException("Operation [" + input + "] is not supported");
        }
    }

    public String doAuditOperation(CodecUsecase usecase, String input) {
        if (input.contains(indexNum(COUNT_CODINGS_BY_DATE))) {
            historyManager.saveToHistory(new HistoryRecord(COUNT_CODINGS_BY_DATE, LocalDate.now()));

            return "Count codings by date:" + audit.countCodingsByDate(usecase);
        } else if (input.contains(indexNum(COUNT_ENCODING_INPUTS))) {
            historyManager.saveToHistory(new HistoryRecord(COUNT_ENCODING_INPUTS, LocalDate.now()));

            return "Count encoding inputs:" + audit.countEncodingInputs();
        } else if (input.contains(indexNum(FIND_MOST_POPULAR_CODEC))) {
            historyManager.saveToHistory(new HistoryRecord(FIND_MOST_POPULAR_CODEC, LocalDate.now()));

            return "Most popular codec :" + audit.findMostPopularCodec(usecase);
        } else {
            throw new OperationUnsupportedException("Operation [" + "] is not supported");
        }
    }

    public String doAuditOperation(CodecUsecase usecase, ContextOperations operation) {
        switch (operation) {
            case COUNT_CODINGS_BY_DATE:
                historyManager.saveToHistory(new HistoryRecord(COUNT_CODINGS_BY_DATE, LocalDate.now()));
                return "Count codings by date:" + audit.countCodingsByDate(usecase);
            case COUNT_ENCODING_INPUTS:
                historyManager.saveToHistory(new HistoryRecord(COUNT_ENCODING_INPUTS, LocalDate.now()));
                return "Count encoding inputs:" + audit.countEncodingInputs();
            case FIND_MOST_POPULAR_CODEC:
                historyManager.saveToHistory(new HistoryRecord(FIND_MOST_POPULAR_CODEC, LocalDate.now()));
                return "Most popular codec :" + audit.findMostPopularCodec(usecase);
            default:
                throw new OperationUnsupportedException("Operation" + operation + " is not supported");
        }
    }

    private void showAudits() {
        System.out.println("\nChoose audit operation: \n");
        for (ContextOperations operation : ContextOperations.values()) {
            if (operation.getTypeMenu().equals("AuditMenu")) {
                System.out.println(operation.getIndex() + ". " + operation.getOperation());
            }
        }
        System.out.println("=> ");
    }

    private void getAuditOperation(CodecUsecase usecase) {
        showAudits();
        String input = scanner.nextLine();
        try {
            System.out.println(doAuditOperation(usecase, input));
        } catch (OperationUnsupportedException | EmptyHistoryException e) {
            logger.error(e);
        }
    }

    private String indexNum(ContextOperations operation) {
        return operation.getIndex();
    }

}

