package org.geekhub.crypto.ui.api.coding.dto;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.ui.console.ContextOperations;

public class CodingResultDto {

    private ContextOperations contextOperations;
    private Algorithms algorithm;
    private String usersInput;
    private String result;

    public CodingResultDto() {
    }

    public void setContextOperations(ContextOperations contextOperations) {
        this.contextOperations = contextOperations;
    }

    public void setAlgorithm(Algorithms algorithm) {
        this.algorithm = algorithm;
    }

    public void setUsersInput(String usersInput) {
        this.usersInput = usersInput;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ContextOperations getContextOperations() {
        return contextOperations;
    }

    public Algorithms getAlgorithm() {
        return algorithm;
    }

    public String getUsersInput() {
        return usersInput;
    }

    public String getResult() {
        return result;
    }
}
