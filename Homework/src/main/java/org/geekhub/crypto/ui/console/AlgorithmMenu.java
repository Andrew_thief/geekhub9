package org.geekhub.crypto.ui.console;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.codecs.Decoder;
import org.geekhub.crypto.codecs.Encoder;
import org.geekhub.crypto.core.exceptions.CodecUnsupportedException;
import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.core.factory.DecodersFactory;
import org.geekhub.crypto.core.factory.EncodersFactory;
import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.geekhub.crypto.util.LoggingFacade;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Scanner;

import static org.geekhub.crypto.core.Algorithms.*;
import static org.geekhub.crypto.ui.console.ContextOperations.DECODE;
import static org.geekhub.crypto.ui.console.ContextOperations.ENCODE;


@Service
public class AlgorithmMenu {
    private Scanner scanner;
    private HistoryManager historyManager;
    private EncodersFactory encodersFactory;
    private DecodersFactory decodersFactory;
    private LoggingFacade logger;

    public AlgorithmMenu(
            HistoryManager historyManager,
            EncodersFactory encodersFactory,
            DecodersFactory decodersFactory,
            LoggingFacade logger) {
        this.scanner = new Scanner(System.in);
        this.historyManager = historyManager;
        this.encodersFactory = encodersFactory;
        this.decodersFactory = decodersFactory;
        this.logger = logger;
    }

    void showAlgorithms(ContextOperations operation) {
        System.out.println("\nChoose algorithm \n");
        int i = 1;
        for (Algorithms algorithm : Algorithms.values()) {
            System.out.println(i + ". " + algorithm.getAlgorithm());
            i++;
        }
        System.out.println("=> ");
        try {
            String algorithm = scanner.nextLine();
            System.out.println("\nEnter message to decode/encode: \n");
            String input = scanner.nextLine();
            System.out.println(getAlgorithm(algorithm, operation, input));
        } catch (CodecUnsupportedException | IllegalInputException e) {
            logger.error(e);
        }
    }

    public String getAlgorithm(String algorithm, ContextOperations operation, String input) {
        if (algorithm.contains(indexNum(CAESAR))
                || algorithm.toLowerCase().contains("caesar")) {
            return doOperationWithAlgorithm(operation, CAESAR, input);
        } else if (algorithm.contains(indexNum(MORSE))
                || algorithm.toLowerCase().contains("morse")) {
            return doOperationWithAlgorithm(operation, MORSE, input);
        } else if (algorithm.contains(indexNum(VIGENERE))
                || algorithm.toLowerCase().contains("vigenere")) {
            return doOperationWithAlgorithm(operation, VIGENERE, input);
        } else if (algorithm.contains(indexNum(CAESAR_OVER_VIGENERE))
                || algorithm.toLowerCase().contains("caesar over vigenere")) {
            return doOperationWithAlgorithm(operation, CAESAR_OVER_VIGENERE, input);
        } else if (algorithm.contains(indexNum(TRANSLATE))
                || algorithm.toLowerCase().contains("translate")) {
            return doOperationWithAlgorithm(operation, TRANSLATE, input);
        } else {
            throw new CodecUnsupportedException("Codec " + algorithm + "is not supported");
        }
    }

    public String doOperationWithAlgorithm(ContextOperations operation, Algorithms algorithm, String input) {

        if (operation.equals(ENCODE)) {
            return "Encoded: " + encodeMethod(algorithm, input);
        } else if (operation.equals(DECODE)) {
            return "Decoded: " + decodeMethod(algorithm, input);
        }
        throw new OperationUnsupportedException("Operation [" + operation + "] is not supported");
    }

    private String encodeMethod(Algorithms algorithm, String input) {

        final Encoder encoder = encodersFactory.getEncoder(algorithm);
        final String encoded = encoder.encode(input);
        historyManager.saveToHistory(new HistoryRecord(algorithm, ENCODE, input, encoded, LocalDate.now()));

        return encoded;
    }

    private String decodeMethod(Algorithms algorithm, String input) {

        final Decoder decoder = decodersFactory.getDecoder(algorithm);
        String decoded = decoder.decode(input);
        historyManager.saveToHistory(new HistoryRecord(algorithm, DECODE, input, decoded, LocalDate.now()));

        return decoded;
    }

    private String indexNum(Algorithms algorithm) {
        String str = "";
        return str + (algorithm.ordinal() + 1);
    }
}