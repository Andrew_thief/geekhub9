package org.geekhub.crypto.ui.config.converters;

import org.geekhub.crypto.core.analytics.CodecUsecase;
import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.springframework.core.convert.converter.Converter;

public class CodecUsecaseConverter implements Converter<String, CodecUsecase> {
    @Override
    public CodecUsecase convert(String source) {
        try {
            return CodecUsecase.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new OperationUnsupportedException("Usecase [" + source + "] not found!");
        }
    }
}
