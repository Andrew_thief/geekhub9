package org.geekhub.crypto.ui.web.controllers;

import org.geekhub.crypto.core.analytics.CodecUsecase;
import org.geekhub.crypto.ui.console.AuditMenu;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/audit")
public class AuditController {

    private AuditMenu auditMenu;

    @Autowired
    public AuditController(AuditMenu auditMenu) {
        this.auditMenu = auditMenu;
    }

    @GetMapping
    public ModelAndView getMenuPage() {
        return new ModelAndView("audit");
    }

    @PostMapping("/{usecase}/{operation}")
    public ModelAndView showAuditResult( @PathVariable CodecUsecase usecase, @PathVariable ContextOperations operation) {
        ModelAndView mav = new ModelAndView("audit");
        mav.addObject("result", auditMenu.doAuditOperation(usecase,operation));
        return mav;
    }

}