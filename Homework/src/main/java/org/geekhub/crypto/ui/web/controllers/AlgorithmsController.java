package org.geekhub.crypto.ui.web.controllers;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.ui.console.AlgorithmMenu;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/algorithms")
public class AlgorithmsController {

    private AlgorithmMenu algorithmMenu;


    public AlgorithmsController(AlgorithmMenu algorithmMenu) {
        this.algorithmMenu = algorithmMenu;
    }


    @GetMapping("/{operation}")
    public ModelAndView getAlgorithms(@PathVariable("operation") ContextOperations operation) {
        return new ModelAndView("algorithms").addObject("operation", operation);
    }

    @PostMapping("/{operation}")
    public ModelAndView doCrypto(@PathVariable ContextOperations operation,
                                 @RequestParam Algorithms algorithm,
                                 @RequestParam String input) {
        ModelAndView mav = new ModelAndView("algorithms");
        mav.addObject("result", algorithmMenu.doOperationWithAlgorithm(operation, algorithm, input));

        return mav;
    }
}
