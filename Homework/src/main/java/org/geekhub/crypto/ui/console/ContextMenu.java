package org.geekhub.crypto.ui.console;

import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.util.LoggingFacade;
import org.springframework.stereotype.Component;

import java.util.Scanner;

import static org.geekhub.crypto.ui.console.ContextOperations.*;

@Component
public class ContextMenu {

    private Scanner scanner;
    private HistoryMenu historyMenu;
    private AlgorithmMenu algorithmMenu;
    private AuditMenu auditMenu;
    private LoggingFacade logger;

    public ContextMenu(
            HistoryMenu historyMenu,
            AlgorithmMenu algorithmMenu,
            AuditMenu auditMenu,
            LoggingFacade logger) {
        this.scanner = new Scanner(System.in);
        this.historyMenu = historyMenu;
        this.algorithmMenu = algorithmMenu;
        this.auditMenu = auditMenu;
        this.logger = logger;
    }

    public void startContextMenu() {
        String input;
        boolean running = true;

        while (running) {
            showOperations();
            input = scanner.nextLine();
            try {
                running = getOperation(input);
                if (!running) {
                    break;
                }
            } catch (OperationUnsupportedException e) {
                logger.warning(e);
            }
        }
    }

    private boolean getOperation(String input) {
        if (input.contains(getIndexNum(ENCODE)) || input.toLowerCase().contains("encode")) {
            algorithmMenu.showAlgorithms(ENCODE);
        } else if (input.contains(getIndexNum(DECODE)) || input.toLowerCase().contains("decode")) {
            algorithmMenu.showAlgorithms(DECODE);
        } else if (input.contains(getIndexNum(HISTORY)) || input.toLowerCase().contains("history")) {
            historyMenu.showHistoryMenu();
        } else if (input.contains(getIndexNum(EXIT)) || input.toLowerCase().contains("exit")) {
            return stopProgram();
        } else if (input.contains(getIndexNum(AUDIT)) || input.toLowerCase().contains("audit")) {
            auditMenu.startAudit();
        } else {
            throw new OperationUnsupportedException("Operation [" + input + "] is not supported");
        }
        return true;
    }

    private void showOperations() {
        System.out.println("\nChoose audit operation: \n");
        for (ContextOperations operation : ContextOperations.values()) {
            if (operation.getTypeMenu().equals("ContextMenu")) {
                System.out.println(operation.getIndex() + ". " + operation.getOperation());
            }
        }
        System.out.println("=> ");
    }

    private String getIndexNum(ContextOperations operation) {
        return operation.getIndex();
    }

    private boolean stopProgram() {
        scanner.close();
        return false;
    }
}