package org.geekhub.crypto.ui.api.audit.dto;

import java.util.Map;

public class CountEncodingInputsDto {

    private Map<String, Integer> usersInputs;

    public CountEncodingInputsDto(Map<String, Integer> usersInputs) {
        this.usersInputs = usersInputs;
    }

    public Map<String, Integer> getUsersInputs() {
        return usersInputs;
    }
}
