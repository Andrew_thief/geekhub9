package org.geekhub.crypto.ui.console;

public enum ContextOperations {

    DECODE("Decoding", "1", "ContextMenu"),
    ENCODE("Encoding", "2", "ContextMenu"),
    AUDIT("Audit", "3", "ContextMenu"),
    HISTORY("History", "4", "ContextMenu"),
    EXIT("Exit", "5", "ContextMenu"),

    COUNT_ENCODING_INPUTS("Count encoding inputs", "1", "AuditMenu"),
    COUNT_CODINGS_BY_DATE("Count codings by date", "2", "AuditMenu"),
    FIND_MOST_POPULAR_CODEC("Find most popular codec", "3", "AuditMenu"),

    SHOW_HISTORY("Show history", "1", "HistoryMenu"),
    CLEAN_HISTORY("Clean history", "2", "HistoryMenu"),
    REMOVE_LAST_ELEMENT("Remove last element", "3", "HistoryMenu");

    private String operation;
    private String index;
    private String typeMenu;

    ContextOperations(String operation, String index, String typeMenu) {
        this.operation = operation;
        this.index = index;
        this.typeMenu = typeMenu;
    }

    public String getOperation() {
        return operation;
    }

    public String getIndex() {
        return index;
    }

    public String getTypeMenu() {
        return typeMenu;
    }
}
