package org.geekhub.crypto.ui.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/menu").permitAll()
                .antMatchers("/encode/**").permitAll()
                .antMatchers("/decode/**").permitAll()
                .antMatchers("/audit/**").hasAuthority("ROLE_ADMIN")
                .antMatchers("/history").hasAuthority("ROLE_USER")
                .antMatchers("/history/show-history").hasAuthority("ROLE_USER")
                .antMatchers("/history/remove-last").hasAuthority("ROLE_ADMIN")
                .antMatchers("/history/clean-all").hasAuthority("ROLE_ADMIN")
                .antMatchers("/api/algorithms/**").permitAll()
                .antMatchers("/api/history").hasAuthority("ROLE_USER")
                .antMatchers("/api/history/remove-last").hasAuthority("ROLE_ADMIN")
                .antMatchers("/api/history/clean-all").hasAuthority("ROLE_ADMIN")
                .antMatchers("/api/audit/**").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and().formLogin().defaultSuccessUrl("/menu");
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        auth.inMemoryAuthentication()
                .passwordEncoder(passwordEncoder)
                .withUser("admin")
                .password(passwordEncoder.encode("admin"))
                .roles("ADMIN");

        auth.inMemoryAuthentication()
                .passwordEncoder(passwordEncoder)
                .withUser("user")
                .password(passwordEncoder.encode("user"))
                .roles("USER");


    }

    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_USER > ROLE_GUEST");
        return roleHierarchy;
    }

}