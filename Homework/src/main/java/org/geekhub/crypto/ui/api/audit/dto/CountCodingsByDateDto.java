package org.geekhub.crypto.ui.api.audit.dto;

import java.time.LocalDate;
import java.util.Map;

public class CountCodingsByDateDto {

    private Map<LocalDate, Long> codingsInDay;

    public CountCodingsByDateDto(Map<LocalDate, Long> codingsInDay) {
        this.codingsInDay = codingsInDay;
    }

    public Map<LocalDate, Long> getCodingsInDay() {
        return codingsInDay;
    }
}
