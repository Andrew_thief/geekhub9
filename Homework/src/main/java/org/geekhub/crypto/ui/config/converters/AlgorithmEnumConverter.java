package org.geekhub.crypto.ui.config.converters;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.exceptions.WrongAlgorithmException;
import org.springframework.core.convert.converter.Converter;


public class AlgorithmEnumConverter implements Converter<String, Algorithms> {

    @Override
    public Algorithms convert(String source) {
        try {
            return Algorithms.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new WrongAlgorithmException("Algorithm [" + source + "] not found!");
        }
    }
}
