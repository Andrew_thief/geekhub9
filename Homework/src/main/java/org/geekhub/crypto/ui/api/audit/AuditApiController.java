package org.geekhub.crypto.ui.api.audit;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.analytics.CodecUsecase;
import org.geekhub.crypto.core.analytics.CodingAudit;
import org.geekhub.crypto.ui.api.audit.dto.CountCodingsByDateDto;
import org.geekhub.crypto.ui.api.audit.dto.CountEncodingInputsDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/audit")
public class AuditApiController {

    private CodingAudit codingAudit;

    public AuditApiController(CodingAudit codingAudit) {
        this.codingAudit = codingAudit;
    }

    @GetMapping("/count-encoding-inputs")
    public CountEncodingInputsDto countEncodingInputs() {
        return new CountEncodingInputsDto(codingAudit.countEncodingInputs());
    }

    @GetMapping("/find-most-popular-codec")
    public Algorithms findMostPopular(@RequestParam CodecUsecase usecase) {
        return codingAudit.findMostPopularCodec(usecase);
    }

    @GetMapping("/count-codings-by-date")
    public CountCodingsByDateDto countCodingsByDate(@RequestParam CodecUsecase usecase) {
        return new CountCodingsByDateDto(codingAudit.countCodingsByDate(usecase));
    }

}
