package org.geekhub.crypto.ui.api.coding;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.ui.api.coding.dto.CodingResultDto;
import org.geekhub.crypto.ui.console.AlgorithmMenu;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/algorithms")
public class AlgorithmsApiController {

    private AlgorithmMenu algorithmMenu;

    public AlgorithmsApiController(AlgorithmMenu algorithmMenu) {
        this.algorithmMenu = algorithmMenu;
    }

    @PostMapping("/encode")
    public CodingResultDto encode(@RequestParam Algorithms algorithm,
                                  @RequestParam String input) {
        CodingResultDto codingResult = new CodingResultDto();
        String result = algorithmMenu.doOperationWithAlgorithm(ContextOperations.ENCODE, algorithm, input);

        codingResult.setAlgorithm(algorithm);
        codingResult.setContextOperations(ContextOperations.ENCODE);
        codingResult.setUsersInput(input);
        codingResult.setResult(result);
        return codingResult;
    }

    @PostMapping("/decode")
    public CodingResultDto decode(@RequestParam Algorithms algorithm,
                                  @RequestParam String input) {
        CodingResultDto codingResult = new CodingResultDto();
        String result = algorithmMenu.doOperationWithAlgorithm(ContextOperations.DECODE, algorithm, input);

        if( algorithmMenu == null){
            System.out.println("null(");
        }
        codingResult.setAlgorithm(algorithm);
        codingResult.setContextOperations(ContextOperations.DECODE);
        codingResult.setUsersInput(input);
        codingResult.setResult(result);
        return codingResult;
    }


}
