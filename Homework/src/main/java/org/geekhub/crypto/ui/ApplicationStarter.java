package org.geekhub.crypto.ui;

import org.geekhub.crypto.ui.config.CodecConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@ComponentScan("org.geekhub.crypto")
@SpringBootApplication(exclude = FlywayAutoConfiguration.class)
@EnableConfigurationProperties
public class ApplicationStarter extends SpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodecConfig.class, args);
    }
}
