package org.geekhub.crypto.ui.config;

import org.geekhub.crypto.ui.config.converters.AlgorithmEnumConverter;
import org.geekhub.crypto.ui.config.converters.CodecUsecaseConverter;
import org.geekhub.crypto.ui.config.converters.ContextOperationsEnumConverter;
import org.geekhub.crypto.ui.web.ControllerLogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan("org.geekhub.crypto")
@PropertySource({"classpath:morseEncode.properties",
        "classpath:morseDecode.properties",
        "classpath:application.properties",
        "classpath:dictionaryEnToUkr.properties",
        "classpath:dictionaryUkrToEn.properties",
        "classpath:keys.properties",
        "classpath:logger.properties",
        "classpath:application.properties"
})
public class CodecConfig implements WebMvcConfigurer {

    private final ControllerLogInterceptor controllerLogInterceptor;

    public CodecConfig(ControllerLogInterceptor controllerLogInterceptor) {
        this.controllerLogInterceptor = controllerLogInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(controllerLogInterceptor);
    }

    @Autowired
    public void dispatcherServlet(DispatcherServlet dispatcherServlet) {
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new AlgorithmEnumConverter());
        registry.addConverter(new ContextOperationsEnumConverter());
        registry.addConverter(new CodecUsecaseConverter());
    }
}
