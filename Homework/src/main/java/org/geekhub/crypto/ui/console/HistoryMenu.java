package org.geekhub.crypto.ui.console;

import org.geekhub.crypto.core.exceptions.OperationUnsupportedException;
import org.geekhub.crypto.core.history.HistoryConsolePrinter;
import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Scanner;

import static org.geekhub.crypto.ui.console.ContextOperations.*;

@Service
public class HistoryMenu {
    private Scanner scanner;
    private HistoryManager historyManager;
    private HistoryConsolePrinter historyPrinter;

    HistoryMenu(HistoryManager historyManager, HistoryConsolePrinter historyPrinter) {
        this.scanner = new Scanner(System.in);
        this.historyManager = historyManager;
        this.historyPrinter = historyPrinter;
    }

    void showHistoryMenu() {
        System.out.println("\nChoose history operations: \n");
        for (ContextOperations operation : ContextOperations.values()) {
            if (operation.getTypeMenu().equals("HistoryMenu")) {
                System.out.println(operation.getIndex() + ". " + operation.getOperation());
            }
        }
        System.out.println("=> ");
        doHistoryOperation(scanner.nextLine());
    }

    public void doHistoryOperation(String input) {
        if (input.contains(getIndexNum(SHOW_HISTORY)) ||
                input.toLowerCase().contains("show")) {
            historyPrinter.print(historyManager.getAllRecords());
        } else if (input.contains(getIndexNum(CLEAN_HISTORY)) ||
                input.toLowerCase().contains("clean")) {
            historyManager.deleteAllRecords();
        } else if (input.contains(getIndexNum(REMOVE_LAST_ELEMENT)) ||
                input.toLowerCase().contains("remove")) {
            historyManager.removeLastRecord();
        } else {
            throw new OperationUnsupportedException("Operation [" + "] is not supported");
        }

        historyManager.saveToHistory(new HistoryRecord(ContextOperations.HISTORY, LocalDate.now()));
    }

    private String getIndexNum(ContextOperations operation) {
        String str = "";
        return str + (operation.getIndex());
    }
}
