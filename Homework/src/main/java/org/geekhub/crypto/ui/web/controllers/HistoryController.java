package org.geekhub.crypto.ui.web.controllers;

import org.geekhub.crypto.core.history.HistoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/history")
public class HistoryController {

    private HistoryManager historyManager;

    @Autowired
    public HistoryController(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    @GetMapping
    public ModelAndView getHistoryPage() {
        return new ModelAndView("history");
    }

    @PostMapping("/show-history")
    public ModelAndView showHistory() {
        return new ModelAndView("history").addObject("result", historyManager.getAllRecords()
        .stream()
        .map(historyRecord ->("[" + historyRecord.getDate() + "]" +" - ["+ historyRecord.getOperationType() +"]"))
        .collect(Collectors.joining()));
    }

    @PostMapping("/remove-last")
    public ModelAndView removeLast() {
        historyManager.removeLastRecord();
        return new ModelAndView("history").addObject("result", "Last element has been removed!");
    }

    @DeleteMapping("/clean-history")
    public ModelAndView cleanHistory() {
        historyManager.deleteAllRecords();
        return new ModelAndView("history").addObject("result", "History has been cleaned!");
    }

}
