package org.geekhub.crypto.ui.api.history;


import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/history")
public class HistoryApiController {

    private HistoryManager historyManager;

    public HistoryApiController(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    @GetMapping
    public List<HistoryRecord> showHistory() {
        return historyManager.getAllRecords();
    }

    @DeleteMapping
    public void cleanHistory() {
        historyManager.deleteAllRecords();
    }

    @DeleteMapping("/remove-last")
    public void removeLastRecord() {
        historyManager.removeLastRecord();
    }
}
