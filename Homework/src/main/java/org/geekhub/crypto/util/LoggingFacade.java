package org.geekhub.crypto.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
public class LoggingFacade {

    private Logger logger;

    public LoggingFacade(Logger logger) {
        this.logger = logger;
    }

    public void info(String inf) {
        logger.log("\n[INFO] [" + LocalDateTime.now() + "] - "
                + inf + "]");
    }

    public void warning(Throwable thr) {
        logger.log("\n[WARNING] [" + LocalDateTime.now() + "] [ "
                + thr.getClass().getSimpleName() + "] - " + thr.getMessage());
    }

    public void error(Throwable thr) {
        logger.log("\n[ERROR!] [" + LocalDateTime.now() + "] ["
                + thr.getClass().getSimpleName() + "] - " + thr.getMessage());
    }

}
