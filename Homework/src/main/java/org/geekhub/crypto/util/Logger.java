package org.geekhub.crypto.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


@Component
@PropertySource("classpath:logger.properties")
public class Logger {

    private static final Path usersDirectory = Paths.get(System.getProperty("user.home"));
    private static final Path logsFile = usersDirectory.resolve("logs.txt");
    private final String loggerType;

    public Logger(@Value("${logger}") String loggerType) {
        this.loggerType = loggerType;
    }

    public void log(String input) {
        if (loggerType.contains("CONSOLE")) {
            print(input);
        }

        if (loggerType.contains("FILE")) {
            write(input);
        }
    }

    private void print(String msg) {
        System.out.println(msg);
    }

    private void write(String msg) {

        try (FileOutputStream out = new FileOutputStream(logsFile.toFile(), true)) {
            out.write(msg.getBytes());
            out.write("\n".getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
