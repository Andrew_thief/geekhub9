--Database: PostgreSQL
----------------------------------------------------

--Creating table for application history:
-- id - identification of record in table
-- operation - menu operation like a ENCODE, AUDIT etc.
-- algorithm - crypto algorithm for coding operations
-- message - users input message to coding
-- result - result of operation
-- dt - date of operation

CREATE TABLE IF NOT EXISTS history (
                       id serial primary key,
                       operation varchar(256) not NULL,
                       algorithm varchar(256) default NULL,
                       message varchar(256) default NULL ,
                       result varchar(256) default NULL ,
                       dt date );