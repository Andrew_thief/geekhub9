CREATE TABLE IF NOT EXISTS popular_en_word (
    id serial primary key,
    word varchar(255) not null
);