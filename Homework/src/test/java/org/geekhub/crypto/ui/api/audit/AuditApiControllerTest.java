package org.geekhub.crypto.ui.api.audit;

import org.geekhub.crypto.core.analytics.CodingAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.util.Collections.emptyMap;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(AuditApiController.class)
public class AuditApiControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private CodingAudit codingAudit;

    @Autowired
    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        when(codingAudit.countEncodingInputs()).thenReturn(emptyMap());
    }


    @Test
    public void Should_Denied_Access_To_Count_Encoding_Inputs_Endpoint_With_Standard_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/count-encoding-inputs")
                        .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Count_Encoding_Inputs_Endpoint_With_Admin_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/count-encoding-inputs")
                        .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    public void Should_Denied_Access_To_Find_Most_Popular_Endpoint_With_Standard_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/find-most-popular-codec")
                        .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Find_Most_Popular_Endpoint_With_Admin_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/find-most-popular-codec")
                        .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    public void Should_Denied_Access_To_Count_Codings_By_Date_Endpoint_With_Standard_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/count-codings-by-date")
                        .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Count_Codings_By_Date_Endpoint_With_Admin_User() throws Exception {
        mockMvc.perform(
                get("/api/audit/count-codings-by-date")
                        .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }
}