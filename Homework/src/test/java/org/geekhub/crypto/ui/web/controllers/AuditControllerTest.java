package org.geekhub.crypto.ui.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuditController.class)
public class AuditControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void Should_Accessed_To_Audit_Endpoint_With_Admin_User() throws Exception {
        mockMvc.perform(get("/audit")
                .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML))
                .andExpect(content().encoding(StandardCharsets.UTF_8.displayName()));
    }

    @Test
    public void Should_Not_Accessed_To_Audit_Endpoint_With_Standard_User() throws Exception {
        mockMvc.perform(get("/audit")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Audit_Encoding_With_Admin_User() throws Exception {
        mockMvc.perform(post("/audit/ENCODING/ENCODE")
                .with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().isOk());
    }

    @Test
    public void Should_Not_Accessed_To_Audit_Encoding_With_Standard_User() throws Exception {
        mockMvc.perform(post("/audit/ENCODING/ENCODE")
                .with(user("user").password("user").roles("USER")))
                .andExpect(status().isForbidden());
    }


}