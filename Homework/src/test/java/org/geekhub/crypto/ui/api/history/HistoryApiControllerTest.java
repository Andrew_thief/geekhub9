package org.geekhub.crypto.ui.api.history;

import org.geekhub.crypto.core.analytics.CodingAudit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.util.Collections.emptyMap;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HistoryApiController.class)
public class HistoryApiControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @MockBean
    private CodingAudit codingAudit;

    @Autowired
    private MockMvc mockMvc;

    @BeforeMethod
    public void setUp() {
        when(codingAudit.countEncodingInputs()).thenReturn(emptyMap());
    }

    @Test
    public void Should_Denied_Access_To_Api_History_Endpoint_Delete_Method_With_Standard_User() throws Exception {
        mockMvc.perform(
                delete("/api/history")
                        .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Api_History_Endpoint_Delete_Method_With_Admin_User() throws Exception {
        mockMvc.perform(
                delete("/api/history")
                        .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

    @Test
    public void Should_Denied_Access_To_Api_History_Remove_Last_Endpoint_Delete_Method_With_Admin_User() throws Exception {
        mockMvc.perform(
                delete("/api/history/remove-last")
                        .with(user("user").password("user").roles("USER"))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void Should_Accessed_To_Api_History_Remove_Last_Endpoint_Delete_Method_With_Admin_User() throws Exception {
        mockMvc.perform(
                delete("/api/history/remove-last")
                        .with(user("admin").password("admin").roles("ADMIN"))
        ).andExpect(status().isOk());
    }

}