package org.geekhub.crypto.core.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThatCode;

@JdbcTest
public class DatabaseConnectivityTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void Should_Execute_JDBC_Command() {
        assertThatCode(() -> jdbcTemplate.execute("select 0")
        ).doesNotThrowAnyException();
    }
}