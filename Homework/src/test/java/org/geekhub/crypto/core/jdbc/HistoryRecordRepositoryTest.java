package org.geekhub.crypto.core.jdbc;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;


@JdbcTest
public class HistoryRecordRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private HistoryRecordRepository historyRecordRepository;

    @BeforeMethod
    public void setUp() {
        historyRecordRepository.deleteAllRecordsFromDatabase();
    }

    @Test
    public void Should_Insert_Record_In_Database() {
        assertThatCode(
                () -> historyRecordRepository.saveRecordToDatabase(
                        new HistoryRecord(
                                Algorithms.CAESAR,
                                ContextOperations.ENCODE,
                                "input",
                                "result",
                                LocalDate.now())))
                .doesNotThrowAnyException();

        assertFalse(historyRecordRepository.getAllHistoryRecords().isEmpty());
    }

    @Test
    public void Should_Not_Throw_Exception_Than_Delete_Last_Empty_Record() {
        assertThatCode(() -> historyRecordRepository.deleteLastRecordFromDatabase())
                .doesNotThrowAnyException();
    }

    @Test
    public void Should_Delete_Last_Element_Than_Table_Has_Record() {
        createRecordInDataBase();

        historyRecordRepository.deleteLastRecordFromDatabase();

        assertEquals(historyRecordRepository.getAllHistoryRecords(),
                List.of());
    }

    @Test
    public void Should_Not_Throw_Exception_Than_Get_Empty_Records() {
        assertThatCode(() -> historyRecordRepository.getAllHistoryRecords())
                .doesNotThrowAnyException();
    }

    @Test
    public void Should_Get_All_Records_Than_Table_Has_Record() {
        createRecordInDataBase();

        assertFalse(historyRecordRepository.getAllHistoryRecords().isEmpty());
    }

    @Test
    public void Should_Not_Throw_Exception_Than_Delete_Empty_Records() {
        assertThatCode(() -> historyRecordRepository.deleteAllRecordsFromDatabase())
                .doesNotThrowAnyException();
    }

    @Test
    public void Should_Delete_All_Records_Than_Table_Has_Records() {
        createRecordInDataBase();
        createRecordInDataBase();

        historyRecordRepository.deleteAllRecordsFromDatabase();

        assertEquals(historyRecordRepository.getAllHistoryRecords(),
                List.of());
    }


    private void createRecordInDataBase() {
        HistoryRecord historyRecord = new HistoryRecord(
                Algorithms.CAESAR,
                ContextOperations.ENCODE,
                "input",
                "result",
                LocalDate.now()
        );

        historyRecordRepository.saveRecordToDatabase(historyRecord);
    }
}
