package org.geekhub.crypto.core.analytics;

import org.geekhub.crypto.core.Algorithms;
import org.geekhub.crypto.core.exceptions.EmptyHistoryException;
import org.geekhub.crypto.core.history.HistoryManager;
import org.geekhub.crypto.core.history.HistoryRecord;
import org.geekhub.crypto.ui.console.ContextOperations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CodingAuditTest {
    private static final String OPERATION_RESULT = "some users message";

    private HistoryManager history;

    private CodingAudit codingAudit;

    @BeforeMethod
    public void setUp() {
        history = mock(HistoryManager.class);

        codingAudit = new CodingAudit(history);
    }

    @Test
    public void Should_Not_Count_Encode_Inputs_When_History_Is_Empty() {
        List<HistoryRecord> emptyHistory = List.of();

        assertEquals(useCountEncodingInputsWith(emptyHistory), Map.of());
    }

    @Test
    public void Should_Not_Count_Encode_Inputs_When_History_Has_A_Decode_Input_Record() {
        List<HistoryRecord> decodeRecords = List.of(createDecodeHistoryRecord("B", Algorithms.CAESAR));

        assertEquals(useCountEncodingInputsWith(decodeRecords), Map.of());
    }

    @Test
    public void Should_Count_Encode_Input_When_History_Has_A_One_Record() {
        Map<String, Integer> expected = Map.of("A", 1);

        List<HistoryRecord> encodeRecords = List.of(createEncodingHistoryRecord("A", Algorithms.VIGENERE));

        assertEquals(useCountEncodingInputsWith(encodeRecords), expected);
    }

    @Test
    public void Should_Count_Encode_Inputs_When_History_Has_A_Multiple_Records() {
        Map<String, Integer> expected = Map.of("A", 2);

        List<HistoryRecord> encodeRecords = List.of(
                createEncodingHistoryRecord("A", Algorithms.CAESAR),
                createEncodingHistoryRecord("A", Algorithms.VIGENERE));

        assertEquals(useCountEncodingInputsWith(encodeRecords), expected);
    }

    @Test
    public void Should_Not_Count_By_Date_When_History_Is_Empty() {
        List<HistoryRecord> emptyHistory = List.of();

        assertEquals(useCountCodingByDateWith(CodecUsecase.ENCODING, emptyHistory), Map.of());
    }

    @Test
    public void Should_Not_Count_By_Date_When_History_Has_Record_With_Another_Usecase() {
        List<HistoryRecord> anotherUsecaseHistory = List.of(
                createDecodeHistoryRecord("B", Algorithms.TRANSLATE));

        assertEquals(useCountCodingByDateWith(CodecUsecase.ENCODING, anotherUsecaseHistory), Map.of());
    }

    @Test
    public void Should_Count_By_Date_When_History_Has_One_Record() {
        List<HistoryRecord> oneRecordHistory = List.of(
                createEncodingHistoryRecord("A", Algorithms.TRANSLATE));

        assertEquals(
                useCountCodingByDateWith(CodecUsecase.ENCODING, oneRecordHistory),
                Map.of(LocalDate.now(), 1L));
    }

    @Test
    public void Should_Count_By_Date_When_History_Has_Multiple_Records() {
        List<HistoryRecord> multiRecordHistory = List.of(
                createEncodingHistoryRecord("A", Algorithms.TRANSLATE),
                createEncodingHistoryRecord("B", Algorithms.MORSE));

        assertEquals(useCountCodingByDateWith(CodecUsecase.ENCODING, multiRecordHistory),
                Map.of(LocalDate.now(), 2L));
    }


    @Test(expectedExceptions = EmptyHistoryException.class)
    public void Should_Not_Find_Most_Popular_Codec_When_History_Is_Empty() {
        List<HistoryRecord> emptyHistory = List.of();

        useFindMostPopularCodecWith(CodecUsecase.ENCODING, emptyHistory);
    }

    @Test(expectedExceptions = EmptyHistoryException.class)
    public void Should_Not_Find_Most_Popular_Codec_When_History_Has_Record_With_Another_Usecase() {
        List<HistoryRecord> emptyHistory = List.of();

        useFindMostPopularCodecWith(CodecUsecase.ENCODING, emptyHistory);
    }

    @Test
    public void Should_Find_Most_Popular_Codec_When_History_Has_One_Record() {
        List<HistoryRecord> oneRecordHistory = List.of(
                createEncodingHistoryRecord("A", Algorithms.CAESAR));


        assertEquals(
                useFindMostPopularCodecWith(CodecUsecase.ENCODING, oneRecordHistory),
                Algorithms.CAESAR);
    }

    @Test
    public void Should_Find_Most_Popular_Codec_When_History_Has_Multiple_Records_With_Another_Usecase() {
        List<HistoryRecord> multiRecordHistory = List.of(
                createEncodingHistoryRecord("A", Algorithms.VIGENERE),
                createDecodeHistoryRecord("B", Algorithms.MORSE),
                createDecodeHistoryRecord("B", Algorithms.MORSE));

        assertEquals(
                useFindMostPopularCodecWith(CodecUsecase.ENCODING, multiRecordHistory),
                Algorithms.VIGENERE);
    }

    @Test
    public void Should_Find_Most_Popular_Codec_When_History_Multiple_Records() {
        List<HistoryRecord> multiRecordHistory = List.of(
                createDecodeHistoryRecord("A", Algorithms.CAESAR),
                createDecodeHistoryRecord("B", Algorithms.CAESAR),
                createDecodeHistoryRecord("B", Algorithms.MORSE));

        assertEquals(
                useFindMostPopularCodecWith(CodecUsecase.DECODING, multiRecordHistory),
                Algorithms.CAESAR);
    }

    private HistoryRecord createEncodingHistoryRecord(String input, Algorithms alg) {
        return new HistoryRecord(
                alg,
                ContextOperations.ENCODE,
                input,
                OPERATION_RESULT,
                LocalDate.now()
        );
    }

    private HistoryRecord createDecodeHistoryRecord(String input, Algorithms alg) {
        return new HistoryRecord(
                alg,
                ContextOperations.DECODE,
                input,
                OPERATION_RESULT,
                LocalDate.now()
        );
    }

    private Map<String, Integer> useCountEncodingInputsWith(List<HistoryRecord> historyRecords) {
        when(history.getAllRecords()).thenReturn(historyRecords);

        return codingAudit.countEncodingInputs();
    }

    private Algorithms useFindMostPopularCodecWith(CodecUsecase usecase, List<HistoryRecord> historyRecords) {
        when(history.getAllRecords()).thenReturn(historyRecords);

        return codingAudit.findMostPopularCodec(usecase);

    }

    private Map<LocalDate, Long> useCountCodingByDateWith(CodecUsecase usecase, List<HistoryRecord> historyRecords) {
        when(history.getAllRecords()).thenReturn(historyRecords);

        return codingAudit.countCodingsByDate(usecase);
    }
}